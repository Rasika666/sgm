﻿namespace SGM
{
	partial class Login
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
			this.pnl_barTop = new System.Windows.Forms.Panel();
			this.btn_minimized = new System.Windows.Forms.PictureBox();
			this.btn_cancel_panel = new System.Windows.Forms.PictureBox();
			this.pnl_menuBar = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pnl_container = new System.Windows.Forms.Panel();
			this.pic_hidechar = new System.Windows.Forms.PictureBox();
			this.pic_showchar = new System.Windows.Forms.PictureBox();
			this.btn_login = new System.Windows.Forms.Button();
			this.pnl_forgetPass = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.pnl_invalidup = new System.Windows.Forms.Panel();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.la = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txt_password = new System.Windows.Forms.TextBox();
			this.txt_username = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.pnl_barTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).BeginInit();
			this.pnl_menuBar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.pnl_container.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_hidechar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pic_showchar)).BeginInit();
			this.pnl_forgetPass.SuspendLayout();
			this.pnl_invalidup.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_barTop
			// 
			this.pnl_barTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.pnl_barTop.Controls.Add(this.btn_minimized);
			this.pnl_barTop.Controls.Add(this.btn_cancel_panel);
			this.pnl_barTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnl_barTop.Location = new System.Drawing.Point(0, 0);
			this.pnl_barTop.Name = "pnl_barTop";
			this.pnl_barTop.Size = new System.Drawing.Size(1124, 40);
			this.pnl_barTop.TabIndex = 0;
			this.pnl_barTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_barTop_MouseDown);
			// 
			// btn_minimized
			// 
			this.btn_minimized.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_minimized.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_minimized.Image = global::SGM.Properties.Resources.minus;
			this.btn_minimized.Location = new System.Drawing.Point(1045, 9);
			this.btn_minimized.Name = "btn_minimized";
			this.btn_minimized.Size = new System.Drawing.Size(25, 25);
			this.btn_minimized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_minimized.TabIndex = 0;
			this.btn_minimized.TabStop = false;
			this.btn_minimized.Click += new System.EventHandler(this.btn_minimized_Click);
			// 
			// btn_cancel_panel
			// 
			this.btn_cancel_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_cancel_panel.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_cancel_panel.Image = global::SGM.Properties.Resources.cancel;
			this.btn_cancel_panel.Location = new System.Drawing.Point(1087, 9);
			this.btn_cancel_panel.Name = "btn_cancel_panel";
			this.btn_cancel_panel.Size = new System.Drawing.Size(25, 25);
			this.btn_cancel_panel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_cancel_panel.TabIndex = 0;
			this.btn_cancel_panel.TabStop = false;
			this.btn_cancel_panel.Click += new System.EventHandler(this.btn_cancel_Click);
			// 
			// pnl_menuBar
			// 
			this.pnl_menuBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.pnl_menuBar.Controls.Add(this.pictureBox1);
			this.pnl_menuBar.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnl_menuBar.Location = new System.Drawing.Point(0, 40);
			this.pnl_menuBar.Name = "pnl_menuBar";
			this.pnl_menuBar.Size = new System.Drawing.Size(479, 501);
			this.pnl_menuBar.TabIndex = 1;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(30, 38);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(408, 411);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// pnl_container
			// 
			this.pnl_container.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.pnl_container.Controls.Add(this.pic_hidechar);
			this.pnl_container.Controls.Add(this.pic_showchar);
			this.pnl_container.Controls.Add(this.btn_login);
			this.pnl_container.Controls.Add(this.pnl_forgetPass);
			this.pnl_container.Controls.Add(this.pnl_invalidup);
			this.pnl_container.Controls.Add(this.txt_password);
			this.pnl_container.Controls.Add(this.txt_username);
			this.pnl_container.Controls.Add(this.label2);
			this.pnl_container.Controls.Add(this.label1);
			this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl_container.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pnl_container.ForeColor = System.Drawing.Color.White;
			this.pnl_container.Location = new System.Drawing.Point(479, 40);
			this.pnl_container.Name = "pnl_container";
			this.pnl_container.Size = new System.Drawing.Size(645, 501);
			this.pnl_container.TabIndex = 2;
			// 
			// pic_hidechar
			// 
			this.pic_hidechar.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pic_hidechar.Image = ((System.Drawing.Image)(resources.GetObject("pic_hidechar.Image")));
			this.pic_hidechar.Location = new System.Drawing.Point(608, 201);
			this.pic_hidechar.Name = "pic_hidechar";
			this.pic_hidechar.Size = new System.Drawing.Size(24, 24);
			this.pic_hidechar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pic_hidechar.TabIndex = 5;
			this.pic_hidechar.TabStop = false;
			this.pic_hidechar.Click += new System.EventHandler(this.pic_hidechar_Click);
			// 
			// pic_showchar
			// 
			this.pic_showchar.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pic_showchar.Image = ((System.Drawing.Image)(resources.GetObject("pic_showchar.Image")));
			this.pic_showchar.Location = new System.Drawing.Point(608, 201);
			this.pic_showchar.Name = "pic_showchar";
			this.pic_showchar.Size = new System.Drawing.Size(24, 24);
			this.pic_showchar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pic_showchar.TabIndex = 5;
			this.pic_showchar.TabStop = false;
			this.pic_showchar.Click += new System.EventHandler(this.pic_showchar_Click);
			// 
			// btn_login
			// 
			this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_login.Font = new System.Drawing.Font("Open Sans SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_login.Location = new System.Drawing.Point(389, 262);
			this.btn_login.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
			this.btn_login.Name = "btn_login";
			this.btn_login.Size = new System.Drawing.Size(179, 37);
			this.btn_login.TabIndex = 4;
			this.btn_login.Text = "Login";
			this.btn_login.UseVisualStyleBackColor = false;
			this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
			// 
			// pnl_forgetPass
			// 
			this.pnl_forgetPass.Controls.Add(this.label4);
			this.pnl_forgetPass.Location = new System.Drawing.Point(371, 444);
			this.pnl_forgetPass.Name = "pnl_forgetPass";
			this.pnl_forgetPass.Size = new System.Drawing.Size(219, 45);
			this.pnl_forgetPass.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
			this.label4.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.label4.Location = new System.Drawing.Point(57, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(140, 23);
			this.label4.TabIndex = 0;
			this.label4.Text = "Forget Password";
			// 
			// pnl_invalidup
			// 
			this.pnl_invalidup.Controls.Add(this.pictureBox2);
			this.pnl_invalidup.Controls.Add(this.la);
			this.pnl_invalidup.Controls.Add(this.label3);
			this.pnl_invalidup.Location = new System.Drawing.Point(59, 330);
			this.pnl_invalidup.Name = "pnl_invalidup";
			this.pnl_invalidup.Size = new System.Drawing.Size(531, 86);
			this.pnl_invalidup.TabIndex = 2;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(507, 5);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(16, 16);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox2.TabIndex = 1;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// la
			// 
			this.la.AutoSize = true;
			this.la.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.la.Location = new System.Drawing.Point(6, 27);
			this.la.Name = "la";
			this.la.Size = new System.Drawing.Size(163, 27);
			this.la.TabIndex = 0;
			this.la.Text = "Please Try Again";
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label3.AutoSize = true;
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.label3.Location = new System.Drawing.Point(3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(0, 27);
			this.label3.TabIndex = 0;
			// 
			// txt_password
			// 
			this.txt_password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_password.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_password.Location = new System.Drawing.Point(59, 190);
			this.txt_password.Name = "txt_password";
			this.txt_password.Size = new System.Drawing.Size(509, 35);
			this.txt_password.TabIndex = 1;
			// 
			// txt_username
			// 
			this.txt_username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_username.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_username.Location = new System.Drawing.Point(59, 78);
			this.txt_username.Name = "txt_username";
			this.txt_username.Size = new System.Drawing.Size(509, 35);
			this.txt_username.TabIndex = 1;
			
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label2.Font = new System.Drawing.Font("Open Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(53, 144);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(122, 33);
			this.label2.TabIndex = 0;
			this.label2.Text = "Password";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label1.Font = new System.Drawing.Font("Open Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(53, 38);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(138, 33);
			this.label1.TabIndex = 0;
			this.label1.Text = "User Name";
			// 
			// Login
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1124, 541);
			this.Controls.Add(this.pnl_container);
			this.Controls.Add(this.pnl_menuBar);
			this.Controls.Add(this.pnl_barTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Login";
			this.Text = "Form1";
			this.pnl_barTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).EndInit();
			this.pnl_menuBar.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.pnl_container.ResumeLayout(false);
			this.pnl_container.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_hidechar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pic_showchar)).EndInit();
			this.pnl_forgetPass.ResumeLayout(false);
			this.pnl_forgetPass.PerformLayout();
			this.pnl_invalidup.ResumeLayout(false);
			this.pnl_invalidup.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_barTop;
		private System.Windows.Forms.Panel pnl_menuBar;
		private System.Windows.Forms.Panel pnl_container;
		private System.Windows.Forms.PictureBox btn_cancel_panel;
		private System.Windows.Forms.PictureBox btn_minimized;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txt_username;
		private System.Windows.Forms.TextBox txt_password;
		private System.Windows.Forms.Panel pnl_invalidup;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label la;
		private System.Windows.Forms.Panel pnl_forgetPass;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btn_login;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pic_showchar;
		private System.Windows.Forms.PictureBox pic_hidechar;
	}
}

