﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.models;
using SGM.database;
using System.IO;

namespace SGM
{
	public partial class Admin_Manage_Teacher : Form
	{
      
        private teacher T1;
		String imageLoc = "";
		public Admin_Manage_Teacher()
		{
			InitializeComponent();
            DB.initializedDB();
        }

        private void loadAll()
        {
            List<teacher> techer1 = teacher.getUsers();

            //clear the list view
            lvDis.Items.Clear();


            foreach (teacher u in techer1)
            {
                ListViewItem item = new ListViewItem(new String[] { u.id.ToString(), u.reg_no, u.tea_name, u.age.ToString(), u.sex, u.address, u.contact_no, u.enter_date.ToString(), u.leave_date.ToString(), u.email });
                item.Tag = u;

                lvDis.Items.Add(item);
            }


        }
            

        private void btnSubmit_Click(object sender, EventArgs e)
        {
          
            String a = txtReg_no.Text;
            String b = txtName.Text;
            int c = Convert.ToInt32(txtAge.Text);
            String d = comboBox1.Text;
            String j = txtAddress.Text;
            String f= txtContact_no.Text;
            String g= dateTimePicker2_enterdate.Value.ToString("yyyy-MM-dd");
            String h =dateTimePicker1_leavedate.Value.ToString("yyyy-MM-dd");
            String i = txtMail.Text;

            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b) ||String.IsNullOrEmpty(d)|| String.IsNullOrEmpty(j) || String.IsNullOrEmpty(f)|| String.IsNullOrEmpty(g)|| String.IsNullOrEmpty(h)|| String.IsNullOrEmpty(i))
            {
                MessageBox.Show("Field is empty!!!");
                return;
            }

            teacher.insert(a,b,c,d,j,f,g,h,i);
            loadAll();

            
           
            txtReg_no.Text = "";
            txtName.Text = "";
            txtAge.Text = "";
            comboBox1.Text = "";
            txtAddress.Text = "";
            txtContact_no.Text = "";
            dateTimePicker2_enterdate.Text = "";
            dateTimePicker1_leavedate.Text = "";
            txtMail.Text = "";
        }

      
        //update buton
        private void button1_Click(object sender, EventArgs e)
        {
            //int y;
           //String a = txtReg_no.Text;
            String b = txtName.Text;
            int c = Convert.ToInt32(txtAge.Text);
            String d = comboBox1.Text;
            String j = txtAddress.Text;
            String f = txtContact_no.Text;
            String g = dateTimePicker2_enterdate.Value.ToString("yyyy-MM-dd");
            String h = dateTimePicker1_leavedate.Value.ToString("yyyy-MM-dd");
            String i = txtMail.Text;

           txtReg_no.Enabled = true;
            if (String.IsNullOrEmpty(b) || String.IsNullOrEmpty(d)|| String.IsNullOrEmpty(j)|| String.IsNullOrEmpty(f)|| String.IsNullOrEmpty(g)|| String.IsNullOrEmpty(h)|| String.IsNullOrEmpty(i))
            {
                MessageBox.Show("Field is empty!!!");
                return;
            }

            T1.update( b, c, d, j, f, g, h, i);
           loadAll();
           
            loadAll();
            txtReg_no.Text = "";
            txtName.Text = "";
            txtAge.Text = "";
            comboBox1.Text = "";
            txtAddress.Text = "";
            txtContact_no.Text = "";
            dateTimePicker2_enterdate.Text = "";
            dateTimePicker1_leavedate.Text = "";
            txtMail.Text = "";
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            // new frmdetails().Show();
             loadAll();

            txtReg_no.Enabled = true;

          
        }

        private void lvDis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvDis.SelectedItems.Count > 0)
            {
                ListViewItem item = lvDis.SelectedItems[0];
                txtReg_no.Enabled = false;
                T1 = (teacher)item.Tag;

                int id = T1.id;
                String a  = T1.reg_no;
                String b = T1.tea_name;
                int c = T1.age;
                String d = T1.sex;
                String j = T1.address;
                String f = T1.contact_no;
                String g = T1.enter_date;
                String h = T1.leave_date;
                String i = T1.email;

				String sql = String.Format("SELECT image FROM teacher_info " +
				"WHERE reg_no = '{0}'", a);

				var record = DB.getData(sql);

				
				//convert datareader to data table
				//then I can fetch one row
				while (record.Read())
				{
					
					byte[] img = (byte[])record["image"];
					//convert binary to image
					MemoryStream ms = new MemoryStream(img);
					pictureBox1.Image = System.Drawing.Image.FromStream(ms);
				}


				txtReg_no.Text = a;
                txtName.Text = b;
                txtAge.Text = c.ToString();
                comboBox1.Text = d.ToString();
                txtAddress.Text = j;
                txtContact_no.Text = f;
                dateTimePicker2_enterdate.Text = g;
                dateTimePicker1_leavedate.Text = h;
                txtMail.Text = i;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int a = T1.id;
          

            txtReg_no.Enabled = true;
          

            T1.delete(a);
            loadAll();
            txtReg_no.Text="";
            txtName.Text = "";
            txtAge.Text ="";
            comboBox1.Text ="";
            txtAddress.Text = "";
            txtContact_no.Text = "";
            dateTimePicker2_enterdate.Text = "";
            dateTimePicker1_leavedate.Text = "";
            txtMail.Text = "";
        }

       
       
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

		private void button2_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				imageLoc = dialog.FileName.ToString();
				pictureBox1.ImageLocation = imageLoc;
			}
			
		}
		public byte[] imgToByteArray(Image img)
		{
			using (MemoryStream mStream = new MemoryStream())
			{
				img.Save(mStream, img.RawFormat);
				return mStream.ToArray();
			}
		}
	}

       
    }

       
    

