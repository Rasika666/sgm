﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using SGM.models;


namespace SGM
{
    public partial class Admin_Manage_Stu : Form
    {
        private student s1;
        public Admin_Manage_Stu()
        {
            InitializeComponent();
            DB.initializedDB();
        }




        private void loadAll()
        {


            // List<student> techer1 = new List<student>();

            //fetch data
            String query = "SELECT * " +
                            "FROM student_info";
            var record = DB.getData(query);
            while (record.Read())
            {

                String reg_no = record["reg_no"].ToString();
                String name_with_initials = record["name_with_initials"].ToString();
                String full_name = record["full_name"].ToString();
                int age = (int)record["age"];
                String sex = record["sex"].ToString();
                String address = record["address"].ToString();
                String contact_no = record["contact_no"].ToString();
                String enter_date = record["enter_date"].ToString();
                String leave_date = record["leave_date"].ToString();

                // List<student> users = student.getUsers();

                ListViewItem item = new ListViewItem(new String[] { reg_no, name_with_initials, full_name, age.ToString(), sex, address, contact_no.ToString(), enter_date.ToString(), leave_date.ToString() });
                //item.Tag = u;

                listView1.Items.Add(item);
            }
        }


        private void btnsubmit_Click(object sender, EventArgs e)

        {

            String a = txtreg.Text;
            String b = txtName.Text;
            String i = txtFullname.Text;
            int c = Convert.ToInt32(txtAge.Text);
            String d = comboBox1.Text;
            String j = txtaddress.Text;
            String f = txtcntactno.Text;


            String g = dateTimePicker1enter.Value.ToString("yyyy-MM-dd");
            String h = dateTimePicker2leave.Value.ToString("yyyy-MM-dd");

            String query = String.Format("INSERT INTO student_info (reg_no,name_with_initials,full_name, age,sex,address,contact_no,enter_date, leave_date )" +
                                       "VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", a, b, i, c, d, j, f, g, h);

            if (DB.putData(query) > 0)
            {
                MessageBox.Show("insert succsecefull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
            }



            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b) || String.IsNullOrEmpty(d) || String.IsNullOrEmpty(j) || String.IsNullOrEmpty(i))
            {
                MessageBox.Show("Field is empty !!!");
                return;
            }
            listView1.Items.Clear();

            loadAll();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadAll();
        }

        public void delete(String reg_no)
        {
            String query = String.Format("DELETE FROM student_info " +
                                        "WHERE reg_no = '{0}'", reg_no);
            if (DB.putData(query) > 0)
            {
                MessageBox.Show("delete Successfull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
                MessageBox.Show(reg_no);
            }
        }



        private void deleteBtn_Click(object sender, EventArgs e)
        {
            String a;
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                a = item.SubItems[0].Text;
                delete(a);
                listView1.Items.Clear();
            }

            loadAll();

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];





                String a = txtreg.Text;
                String b = txtName.Text;
                String i = txtFullname.Text;
                String c = txtAge.Text;
                String d = comboBox1.Text;
                String j = txtaddress.Text;
                String f = txtcntactno.Text;
            }

        }

        public void update(String a, String b, String i, String c, String d, String j, String f, String g, String h)
        {
            String query = String.Format("UPDATE student_info " +
                                         "SET reg_no=a,name_with_initials=b, full_name=i, age=c, sex=d, address=j, contact_no=f, enter_date=g, leave_date=h" +
                                        "WHERE reg_no =a");
            if (DB.putData(query) > 0)
            {
                MessageBox.Show("Update Successfull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
            }
        }
        /*private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                EmpIDtextBox.Text = item.SubItems[0].Text;
                EmpNametextBox.Text = item.SubItems[1].Text;
            }*/

        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                String a = item.SubItems[0].Text;
                String b = item.SubItems[1].Text;
                String i = item.SubItems[2].Text;
                String c = item.SubItems[3].Text;

                String d = item.SubItems[4].Text;
                String j = item.SubItems[5].Text;
                String f = item.SubItems[6].Text;
                String g = item.SubItems[7].Text;
                String h = item.SubItems[8].Text;

                update(a, b, i, c, d, j, f, g, h);






            }
        }

        
    }
}
