﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using SGM.models;
using SGM.database;

namespace SGM
{
	public partial class Login : Form
	{
		private Login_user currentLoger;

		public Login()
		{
			InitializeComponent();
			// Set to no text.  
			txt_password.Text = "";
			// The password character is an asterisk.  
			txt_password.PasswordChar = '*';
			// The control will allow no more than 14 characters.  
			txt_password.MaxLength = 15;

			pnl_forgetPass.Hide();
			pnl_invalidup.Hide();

			//db initialization
			DB.initializedDB();

			//forcus the username
			this.ActiveControl = txt_username;

			//eye
			pic_hidechar.Hide();
			
		}

		

		private void btn_minimized_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		

		private void btn_cancel_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		

		[DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
		private extern static void ReleaseCapture();
		[DllImport("user32.DLL", EntryPoint = "SendMessage")]

		private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

		private void pnl_barTop_MouseDown(object sender, MouseEventArgs e)
		{
			ReleaseCapture();
			SendMessage(this.Handle, 0x112, 0xf012, 0);
		}

		private void btn_off_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void toContainer(object ob)
		{
			if (this.pnl_container.Controls.Count > 0)
				this.pnl_container.Controls.RemoveAt(0);

			Form fh = ob as Form;
			fh.TopLevel = false;
			fh.Dock = DockStyle.Fill;
			this.pnl_container.Controls.Add(fh);
			this.pnl_container.Tag = fh;
			fh.Show();
		}

		private void btn_login_Click(object sender, EventArgs e)
		{
			String username = txt_username.Text;
			String password = txt_password.Text;

			//check user name or password is empty
			if(username.Equals("") || password.Equals(""))
			{
				clean_text();
				label3.Text = "User Name or Password cannot be Empty";
				this.ActiveControl = txt_username;
				pnl_invalidup.Show();
				return;
			}

			//get all users
			List<Login_user> users = Login_user.getUsers(username);

			//check no data 
			if (users.Count == 0)
			{
				clean_text();
				label3.Text = "Invalid User Name or Password";
				pnl_invalidup.Show();
				this.ActiveControl = txt_username;
				return;
			}
			currentLoger = users[0];
			
			//now username is correct
			//check for password and allow enter futher
			if(currentLoger.password != password)
			{
				clean_text();
				label3.Text = "Invalid User Name or Password";
				pnl_invalidup.Show();
				pnl_forgetPass.Show();
				this.ActiveControl = txt_username;
				return;
			}

			//sucess scenario
			if (currentLoger.member_state == "student")
			{
				var student = new layout_stu(currentLoger.reg_no);
				student.Show();
				layout_stu.regno = currentLoger.reg_no;
				
				student.loadAvatorDetails(currentLoger.reg_no);


			}
			else if(currentLoger.member_state == "teacher")
			{
				var teacher = new layout_teacher(currentLoger.reg_no);
				teacher.Show();
				teacher.loadAvatorDetails(currentLoger.reg_no);
			}
			else if (currentLoger.member_state == "admin")
			{
			
				var admin = new layout_admin();
				admin.Show();
				admin.loadAvatorDetails(currentLoger.reg_no);
			}
			else
			{
				MessageBox.Show("something went wrong");
			}

			this.Hide();
				
		}

		private void clean_text()
		{
			txt_username.Text = "";
			txt_password.Text = "";
		}

		private void pictureBox2_Click(object sender, EventArgs e)
		{
			pnl_invalidup.Hide();
		}

		private void pic_showchar_Click(object sender, EventArgs e)
		{
			
			txt_password.PasswordChar = '\0';
			pic_showchar.Hide();
			pic_hidechar.Show();
		}

		private void pic_hidechar_Click(object sender, EventArgs e)
		{

			txt_password.PasswordChar = '*';
			pic_hidechar.Hide();
			pic_showchar.Show();

		}

		
	}
}
