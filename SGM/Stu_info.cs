﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using SGM.models;

namespace SGM
{
	public partial class Stu_info : Form
	{
		private string regno;
		public Stu_info(String regno)
		{
			this.regno = regno;
			InitializeComponent();
			init_disable_txt();
			DB.initializedDB();
			load_txt();
			pnl_login.Hide();
			pnl_msg.Hide();
		}

		String imageLoc = "";

		

		private void init_disable_txt()
		{
			txt_address.ReadOnly = true;
			txt_age.ReadOnly = true;
			txt_contact.ReadOnly = true;
			txt_full_name.ReadOnly = true;
			txt_mail.ReadOnly = true;
			txt_name_init.ReadOnly = true;
		}

		private void btn_browse_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				imageLoc = dialog.FileName.ToString();
				pic_avator.ImageLocation = imageLoc;
			}
		}
		private void load_txt()
		{
			List<Stu_info_model> student = Stu_info_model.getStu(this.regno);



			txt_reg.Text = this.regno;
			txt_name_init.Text = student[0].name_with_initials;
			txt_full_name.Text = student[0].full_name;
			txt_age.Text = student[0].age;
			//sex here
			if (student[0].sex== "male")
				{
					radioButton1.Checked = true;
				}
			else
				{
					radioButton2.Checked = true;
				}
			txt_address.Text = student[0].Address;
			txt_contact.Text = student[0].contact_no;
				//load img
			byte[] img = student[0].image;
			//convert binary to image
			MemoryStream ms = new MemoryStream(img);
			pic_avator.Image = System.Drawing.Image.FromStream(ms);
			txt_enterdate.Text = student[0].enter_date.ToString();
			 if(student[0].Leave_date.ToString() == DateTime.MinValue.ToString())
			{
				txt_leavedate.Text = "";
			}
			else
			{
				txt_leavedate.Text = student[0].Leave_date.ToString();
			}
			txt_mail.Text = student[0].email;

		}

		private void update_student()
		{
			String reg_no = txt_reg.Text;
			String init = txt_name_init.Text;
			String fn = txt_full_name.Text;
			String age = txt_age.Text;
			//sex
			String sex = "";
			if(radioButton1.Checked == true)
			{
				sex = "male";
			}
			if (radioButton2.Checked == true)
			{
				sex = "female";
			}
			String add = txt_address.Text;
			String contact = txt_contact.Text;
			//image
			byte[] img = imgToByteArray(pic_avator.Image);
			//dates theDate.ToString("yyyy-MM-dd H:mm:ss");
			DateTime edate = DateTime.Parse(txt_enterdate.Text);
			DateTime ldate;
			if (string.IsNullOrEmpty(txt_leavedate.Text))
			{
				ldate = DateTime.MinValue;
			}
			else
			{
				ldate = DateTime.Parse(txt_leavedate.Text);
			}
			String email = txt_mail.Text;

			Stu_info_model updateStu = new Stu_info_model(reg_no,init,fn,age,sex,add,contact,img,edate,ldate,email);

			updateStu.updateStu();
		}

		public byte[] imgToByteArray(Image img)
		{
			using (MemoryStream mStream = new MemoryStream())
			{
				img.Save(mStream, img.RawFormat);
				return mStream.ToArray();
			}
		}

		private void btn_edit_Click(object sender, EventArgs e)
		{
			txt_address.ReadOnly = false;
			txt_age.ReadOnly = false;
			txt_contact.ReadOnly = false;
			txt_full_name.ReadOnly = false;
			txt_mail.ReadOnly = false;
			txt_name_init.ReadOnly = false;
		}

		private void btn_login_Click(object sender, EventArgs e)
		{
			pnl_login.Show();
		}

		private void btn_update_Click(object sender, EventArgs e)
		{
			update_student();
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			pnl_login.Hide();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.ActiveControl = txt_cuser;
			pnl_msg.Hide();
			if (String.IsNullOrEmpty(txt_cuser.Text) || String.IsNullOrEmpty(txt_nuser.Text) ||
				String.IsNullOrEmpty(txt_cpass.Text) || String.IsNullOrEmpty(txt_npass.Text))
			{
				clear_login_txt();
				pnl_msg.Show();
				lbl_msg.Text = "Empty Field Exist!!!";
				this.ActiveControl = txt_cuser;
				return;
			}


			// check password s
			if (txt_npass.Text != txt_conpass.Text)
			{
				clear_login_txt();
				pnl_msg.Show();
				lbl_msg.Text = "Two different passwords";
				this.ActiveControl = txt_cuser;
				return;
			}

			//check current user name and cirrent password is ok
			List<Login_user> users = Login_user.getUsers(txt_cuser.Text);
			if(users.Count == 0)
			{
				clear_login_txt();
				pnl_msg.Show();
				lbl_msg.Text = "Incorrect user name or password";
				this.ActiveControl = txt_cuser;
				return;
			}

			//check enter user name is already exists 
			String sql = "SELECT user_name FROM login";
			var rec = DB.getData(sql);
			
			while (rec.Read())
			{
				if(rec["user_name"].ToString() == txt_nuser.Text)
				{
					clear_login_txt();
					pnl_msg.Show();
					lbl_msg.Text = "User Name is already there!!!";
					this.ActiveControl = txt_cuser;
					return; ;
				}	

			}

			

			//success one
			 sql = "UPDATE login SET user_name = '"+txt_nuser.Text+"', password = '"+txt_npass.Text+"' where reg_no = '"+this.regno+"'";

			if (DB.putData(sql) > 0)
			{
				clear_login_txt();
				MessageBox.Show("Update successfully!!!");

			}
		}

		private void clear_login_txt()
		{
			txt_cuser.Text = "";
			txt_cpass.Text = "";
			txt_npass.Text = "";
			txt_nuser.Text = "";
			txt_conpass.Text = "";

		}

		private void pictureBox2_Click(object sender, EventArgs e)
		{
			pnl_msg.Hide();
		}
	}
}
