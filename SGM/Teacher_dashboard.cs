﻿using SGM.database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGM
{
	public partial class Teacher_dashboard : Form
	{
		private static String regno;
		public Teacher_dashboard(String reg_no)
		{
			regno = reg_no;
			InitializeComponent();
			DB.initializedDB();
			load_dash();
		}

		private void load_dash()
		{
			String sql = "SELECT * FROM teacher_info WHERE reg_no = '" + regno + "'";
			var record = DB.getData(sql);

			while (record.Read())
			{
				byte[] img = (byte[])record["image"];
				//convert binary to image
				MemoryStream ms = new MemoryStream(img);
				pic_avatar.Image = System.Drawing.Image.FromStream(ms);
				lbl_name.Text = record["tea_name"].ToString();
				lbl_age.Text = record["age"].ToString();
				lbl_sex.Text = record["sex"].ToString();
				lbl_contact.Text = record["contact_no"].ToString();
			}

		}
	}
}
