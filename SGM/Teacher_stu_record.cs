﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using System.Windows.Forms.DataVisualization.Charting;

namespace SGM
{
    public partial class Teacher_stu_record : Form
    {
        string grade;
        string sql2;
        string gradeval;
        string subject = "";
        string student = "";
       
        public Teacher_stu_record()
        {
            InitializeComponent();
        }

        private void load_comboBox()
        {
            switch (grade)
            {
                case "6":
                    gradeval = "grade6";
                    break;
                case "7":
                    gradeval = "grade7";
                    break;
                case "8":
                    gradeval = "grade8";
                    break;
                case "9":
                    gradeval = "grade9";
                    break;
                case "10":
                    gradeval = "grade10";
                    break;
                case "11":
                    gradeval = "grade11";
                    break;

                default:
                    break;
            }

            //filling student
            string sql1 = String.Format("SELECT reg_no " +
                                     "FROM  current_stu_grade " +
                                     "WHERE grade = "+ grade);
            var recode1 = DB.getData(sql1);
            while (recode1.Read())
            {
                cmbStudent.Items.Add(recode1["reg_no"]);
            }

            //filling subject
            sql2 = String.Format("SELECT {0} FROM grade_subjects WHERE {0} IS NOT NULL",gradeval);
            var recode2 = DB.getData(sql2);

            while (recode2.Read())
            {
                cmbSubject.Items.Add(recode2[gradeval]);
            }
        }
        private void cmbGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            grade = cmbGrade.SelectedItem.ToString();
            load_comboBox();
        }

        private void loadChart( string Reg_no,string subject) {
            foreach (var series in chartStudent.Series)
            {
                series.Points.Clear();
            }
            chartStudent.Series.Clear();

            //create chart
            var chart = chartStudent.ChartAreas[0];
            chartStudent.ChartAreas[0].AxisX.LabelStyle.Angle = 45;
            chart.AxisX.IntervalType = DateTimeIntervalType.Number;

            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;

            chart.AxisX.Minimum = 1;
            //chart.AxisX.Maximum = 24;
            chart.AxisY.Minimum = 0;
            //chart.AxisY.Maximum = 100;
            chart.AxisX.Interval = 1;
            chart.AxisY.Interval = 5;
            //line for current student
            chartStudent.Series.Add(Reg_no);
            chartStudent.Series[Reg_no].ChartType = SeriesChartType.Spline;
            chartStudent.Series[Reg_no].Color = Color.Green;
            chartStudent.Series[0].IsVisibleInLegend = false;
            

            String sql4 = String.Format("SELECT {0} as y, CONCAT('grade',grade,' ', 'term',sem) as x " +
                                        "FROM  marks " +
                                        "WHERE reg_no = '{1}'" +
                                        "ORDER 	BY	grade,sem", subject, Reg_no);
            
            var record4 = DB.getData(sql4);
            try {
                while (record4.Read())
                {
                    chartStudent.Series[Reg_no].Points.AddXY(record4["x"], record4["y"]);
                }
            }
            catch (Exception) {
                lblerr.Text = subject+" is not done by "+student;
            }
            

        }

        private void cmbStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            student = cmbStudent.SelectedItem.ToString();
            if(subject != "")
            {
                lblerr.Text = "";
                loadChart(student,subject);
            }else
                lblerr.Text = "please select a subject";
        }

        private void cmbSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            subject = cmbSubject.SelectedItem.ToString();
            if(student != "")
            {
                lblerr.Text = "";
                loadChart(student,subject);
            }
            else
                lblerr.Text = "please select a student";
        }
    }
}