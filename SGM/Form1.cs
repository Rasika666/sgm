﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SGM
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog of1 = new OpenFileDialog();
                of1.Filter = "Excel Files | *.xlsx; *.xls; *.xlsm";
                if (of1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.txtpath.Text = of1.FileName;
                }
                string constr = "Provider= Microsoft.ACE.OLEDB.12.0;Data Source=" + txtpath.Text + ";Extended Properties= \"Excel 12.0; HDR=YES;\";";
                OleDbConnection con = new OleDbConnection(constr);
                con.Open();
                comboBox1.DataSource = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                comboBox1.DisplayMember = "TABLE_NAME";
                comboBox1.ValueMember = "TABLE_NAME";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string constr= "Provider= Microsoft.ACE.OLEDB.12.0;Data Source=" + txtpath.Text + ";Extended Properties= \"Excel 12.0; HDR=YES;\";";
                OleDbConnection con = new OleDbConnection(constr);
                OleDbDataAdapter sda = new OleDbDataAdapter("select * from[" + comboBox1.SelectedValue + "]", con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                foreach(DataRow row in dt.Rows)
                {
                    dataGridView1.DataSource = dt;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            database.DB.initializedDB();

            for(int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                string sql = "INSERT INTO sgm.marks VALUES ('" + dataGridView1.Rows[i].Cells[0].Value + "','" +
                                                              dataGridView1.Rows[i].Cells[1].Value + "','" +
                                                              dataGridView1.Rows[i].Cells[2].Value + "','" +
                                                              dataGridView1.Rows[i].Cells[3].Value + "')";
                database.DB.putData(sql);
            }
            database.DB.closeConnection();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
        }
    }
}
