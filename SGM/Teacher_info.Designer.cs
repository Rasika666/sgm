﻿namespace SGM
{
	partial class Teacher_info
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.txt_regno = new System.Windows.Forms.TextBox();
			this.txt_name = new System.Windows.Forms.TextBox();
			this.txt_age = new System.Windows.Forms.TextBox();
			this.txt_address = new System.Windows.Forms.TextBox();
			this.txt_contact = new System.Windows.Forms.TextBox();
			this.txt_email = new System.Windows.Forms.TextBox();
			this.txt_edate = new System.Windows.Forms.TextBox();
			this.txt_ldate = new System.Windows.Forms.TextBox();
			this.pb_avatar = new System.Windows.Forms.PictureBox();
			this.btn_browse = new System.Windows.Forms.Button();
			this.btn_edit = new System.Windows.Forms.Button();
			this.btn_update = new System.Windows.Forms.Button();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pb_avatar)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Teacher Information";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 6;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
			this.tableLayoutPanel1.Controls.Add(this.label2, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label3, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label4, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.label5, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.label6, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.label7, 1, 6);
			this.tableLayoutPanel1.Controls.Add(this.label8, 1, 7);
			this.tableLayoutPanel1.Controls.Add(this.label9, 1, 8);
			this.tableLayoutPanel1.Controls.Add(this.label10, 3, 8);
			this.tableLayoutPanel1.Controls.Add(this.txt_regno, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_name, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.txt_age, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.txt_address, 2, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_contact, 2, 6);
			this.tableLayoutPanel1.Controls.Add(this.txt_email, 2, 7);
			this.tableLayoutPanel1.Controls.Add(this.txt_edate, 2, 8);
			this.tableLayoutPanel1.Controls.Add(this.txt_ldate, 4, 8);
			this.tableLayoutPanel1.Controls.Add(this.pb_avatar, 4, 1);
			this.tableLayoutPanel1.Controls.Add(this.btn_browse, 4, 5);
			this.tableLayoutPanel1.Controls.Add(this.btn_edit, 2, 9);
			this.tableLayoutPanel1.Controls.Add(this.btn_update, 4, 9);
			this.tableLayoutPanel1.Controls.Add(this.radioButton1, 2, 4);
			this.tableLayoutPanel1.Controls.Add(this.radioButton2, 3, 4);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 10;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(835, 487);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label2.Location = new System.Drawing.Point(23, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(78, 27);
			this.label2.TabIndex = 0;
			this.label2.Text = "Reg No";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label3.Location = new System.Drawing.Point(23, 70);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 27);
			this.label3.TabIndex = 0;
			this.label3.Text = "Name";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label4.Location = new System.Drawing.Point(23, 120);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(47, 27);
			this.label4.TabIndex = 0;
			this.label4.Text = "Age";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label5.Location = new System.Drawing.Point(23, 170);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(44, 27);
			this.label5.TabIndex = 0;
			this.label5.Text = "Sex";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label6.Location = new System.Drawing.Point(23, 220);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 27);
			this.label6.TabIndex = 0;
			this.label6.Text = "Address";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label7.Location = new System.Drawing.Point(23, 270);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(84, 27);
			this.label7.TabIndex = 0;
			this.label7.Text = "Contact";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label8.Location = new System.Drawing.Point(23, 320);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(63, 27);
			this.label8.TabIndex = 0;
			this.label8.Text = "Email";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label9.Location = new System.Drawing.Point(23, 370);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(110, 27);
			this.label9.TabIndex = 0;
			this.label9.Text = "Enter Date";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label10.Location = new System.Drawing.Point(419, 370);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(114, 27);
			this.label10.TabIndex = 0;
			this.label10.Text = "Leave Date";
			// 
			// txt_regno
			// 
			this.txt_regno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_regno, 2);
			this.txt_regno.Enabled = false;
			this.txt_regno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_regno.Location = new System.Drawing.Point(221, 23);
			this.txt_regno.Name = "txt_regno";
			this.txt_regno.Size = new System.Drawing.Size(390, 30);
			this.txt_regno.TabIndex = 1;
			// 
			// txt_name
			// 
			this.txt_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_name, 2);
			this.txt_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_name.Location = new System.Drawing.Point(221, 73);
			this.txt_name.Name = "txt_name";
			this.txt_name.Size = new System.Drawing.Size(390, 30);
			this.txt_name.TabIndex = 2;
			// 
			// txt_age
			// 
			this.txt_age.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_age, 2);
			this.txt_age.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_age.Location = new System.Drawing.Point(221, 123);
			this.txt_age.Name = "txt_age";
			this.txt_age.Size = new System.Drawing.Size(390, 30);
			this.txt_age.TabIndex = 2;
			// 
			// txt_address
			// 
			this.txt_address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_address, 2);
			this.txt_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_address.Location = new System.Drawing.Point(221, 223);
			this.txt_address.Name = "txt_address";
			this.txt_address.Size = new System.Drawing.Size(390, 30);
			this.txt_address.TabIndex = 2;
			// 
			// txt_contact
			// 
			this.txt_contact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_contact, 2);
			this.txt_contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_contact.Location = new System.Drawing.Point(221, 273);
			this.txt_contact.Name = "txt_contact";
			this.txt_contact.Size = new System.Drawing.Size(390, 30);
			this.txt_contact.TabIndex = 2;
			// 
			// txt_email
			// 
			this.txt_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_email, 2);
			this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_email.Location = new System.Drawing.Point(221, 323);
			this.txt_email.Name = "txt_email";
			this.txt_email.Size = new System.Drawing.Size(390, 30);
			this.txt_email.TabIndex = 2;
			// 
			// txt_edate
			// 
			this.txt_edate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_edate.Enabled = false;
			this.txt_edate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_edate.Location = new System.Drawing.Point(221, 373);
			this.txt_edate.Name = "txt_edate";
			this.txt_edate.Size = new System.Drawing.Size(192, 30);
			this.txt_edate.TabIndex = 2;
			// 
			// txt_ldate
			// 
			this.txt_ldate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_ldate.Enabled = false;
			this.txt_ldate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_ldate.Location = new System.Drawing.Point(617, 373);
			this.txt_ldate.Name = "txt_ldate";
			this.txt_ldate.Size = new System.Drawing.Size(192, 30);
			this.txt_ldate.TabIndex = 2;
			// 
			// pb_avatar
			// 
			this.pb_avatar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pb_avatar.Location = new System.Drawing.Point(617, 23);
			this.pb_avatar.Name = "pb_avatar";
			this.tableLayoutPanel1.SetRowSpan(this.pb_avatar, 4);
			this.pb_avatar.Size = new System.Drawing.Size(192, 194);
			this.pb_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pb_avatar.TabIndex = 3;
			this.pb_avatar.TabStop = false;
			// 
			// btn_browse
			// 
			this.btn_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_browse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_browse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_browse.Font = new System.Drawing.Font("Open Sans SemiBold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_browse.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_browse.Location = new System.Drawing.Point(617, 223);
			this.btn_browse.Name = "btn_browse";
			this.btn_browse.Size = new System.Drawing.Size(192, 44);
			this.btn_browse.TabIndex = 4;
			this.btn_browse.Text = "Browse";
			this.btn_browse.UseVisualStyleBackColor = false;
			this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
			// 
			// btn_edit
			// 
			this.btn_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.tableLayoutPanel1.SetColumnSpan(this.btn_edit, 2);
			this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_edit.Font = new System.Drawing.Font("Open Sans SemiBold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_edit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_edit.Location = new System.Drawing.Point(221, 423);
			this.btn_edit.Name = "btn_edit";
			this.btn_edit.Size = new System.Drawing.Size(390, 36);
			this.btn_edit.TabIndex = 5;
			this.btn_edit.Text = "Edit";
			this.btn_edit.UseVisualStyleBackColor = false;
			this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
			// 
			// btn_update
			// 
			this.btn_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_update.Font = new System.Drawing.Font("Open Sans SemiBold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_update.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_update.Location = new System.Drawing.Point(699, 423);
			this.btn_update.Name = "btn_update";
			this.btn_update.Size = new System.Drawing.Size(110, 36);
			this.btn_update.TabIndex = 6;
			this.btn_update.Text = "Update";
			this.btn_update.UseVisualStyleBackColor = false;
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioButton1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.radioButton1.Location = new System.Drawing.Point(221, 173);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(78, 31);
			this.radioButton1.TabIndex = 7;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Male";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioButton2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.radioButton2.Location = new System.Drawing.Point(419, 173);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(100, 31);
			this.radioButton2.TabIndex = 7;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Female";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// Teacher_info
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Teacher_info";
			this.Text = "Teacher_info";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pb_avatar)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txt_regno;
		private System.Windows.Forms.TextBox txt_name;
		private System.Windows.Forms.TextBox txt_age;
		private System.Windows.Forms.TextBox txt_address;
		private System.Windows.Forms.TextBox txt_contact;
		private System.Windows.Forms.TextBox txt_email;
		private System.Windows.Forms.TextBox txt_edate;
		private System.Windows.Forms.TextBox txt_ldate;
		private System.Windows.Forms.PictureBox pb_avatar;
		private System.Windows.Forms.Button btn_browse;
		private System.Windows.Forms.Button btn_edit;
		private System.Windows.Forms.Button btn_update;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
	}
}