﻿namespace SGM
{
	partial class Teacher_dashboard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.pic_avatar = new OvalPictureBox();
			this.lbl_name = new System.Windows.Forms.Label();
			this.lbl_age = new System.Windows.Forms.Label();
			this.lbl_sex = new System.Windows.Forms.Label();
			this.lbl_contact = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avatar)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Dashboard";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.ColumnCount = 20;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.Controls.Add(this.pic_avatar, 7, 1);
			this.tableLayoutPanel1.Controls.Add(this.lbl_name, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.lbl_age, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.lbl_sex, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.lbl_contact, 1, 5);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 32);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 7;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(835, 470);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// pic_avatar
			// 
			this.pic_avatar.BackColor = System.Drawing.Color.DarkGray;
			this.tableLayoutPanel1.SetColumnSpan(this.pic_avatar, 6);
			this.pic_avatar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pic_avatar.Location = new System.Drawing.Point(290, 23);
			this.pic_avatar.Name = "pic_avatar";
			this.pic_avatar.Size = new System.Drawing.Size(240, 194);
			this.pic_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pic_avatar.TabIndex = 0;
			this.pic_avatar.TabStop = false;
			// 
			// lbl_name
			// 
			this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_name.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.lbl_name, 18);
			this.lbl_name.Font = new System.Drawing.Font("Open Sans SemiBold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.lbl_name.Location = new System.Drawing.Point(44, 220);
			this.lbl_name.Name = "lbl_name";
			this.lbl_name.Size = new System.Drawing.Size(732, 50);
			this.lbl_name.TabIndex = 1;
			this.lbl_name.Text = "Full Name";
			this.lbl_name.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lbl_age
			// 
			this.lbl_age.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_age.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.lbl_age, 18);
			this.lbl_age.Font = new System.Drawing.Font("Open Sans SemiBold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_age.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.lbl_age.Location = new System.Drawing.Point(44, 270);
			this.lbl_age.Name = "lbl_age";
			this.lbl_age.Size = new System.Drawing.Size(732, 50);
			this.lbl_age.TabIndex = 1;
			this.lbl_age.Text = "age";
			this.lbl_age.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lbl_sex
			// 
			this.lbl_sex.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_sex.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.lbl_sex, 18);
			this.lbl_sex.Font = new System.Drawing.Font("Open Sans SemiBold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_sex.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.lbl_sex.Location = new System.Drawing.Point(44, 320);
			this.lbl_sex.Name = "lbl_sex";
			this.lbl_sex.Size = new System.Drawing.Size(732, 50);
			this.lbl_sex.TabIndex = 1;
			this.lbl_sex.Text = "sex";
			this.lbl_sex.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lbl_contact
			// 
			this.lbl_contact.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_contact.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.lbl_contact, 18);
			this.lbl_contact.Font = new System.Drawing.Font("Open Sans SemiBold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_contact.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.lbl_contact.Location = new System.Drawing.Point(44, 370);
			this.lbl_contact.Name = "lbl_contact";
			this.lbl_contact.Size = new System.Drawing.Size(732, 50);
			this.lbl_contact.TabIndex = 1;
			this.lbl_contact.Text = "contact";
			this.lbl_contact.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Teacher_dashboard
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Teacher_dashboard";
			this.Text = "Teacher_dashboard";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avatar)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private OvalPictureBox pic_avatar;
		private System.Windows.Forms.Label lbl_name;
		private System.Windows.Forms.Label lbl_age;
		private System.Windows.Forms.Label lbl_sex;
		private System.Windows.Forms.Label lbl_contact;
	}
}