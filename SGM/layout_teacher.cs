﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using SGM.database;

namespace SGM
{
	public partial class layout_teacher : Form
	{
		private String tea_regno;
		public layout_teacher(String regno)
		{
			InitializeComponent();
			toContainer(new Teacher_dashboard(regno));
			this.tea_regno = regno;
			panel1.Show();
			panel2.Hide();
			panel3.Hide();
			panel4.Hide();
			panel5.Hide();
			panel6.Hide();
		}


		public void loadAvatorDetails(String reg_no)
		{
			//for now we load only name
			String sql = String.Format("SELECT 	tea_name, image " +
										"FROM  teacher_info " +
										"WHERE reg_no = '{0}'", reg_no);
			var record = DB.getData(sql);

			while (record.Read())
			{
				label1.Text = record["tea_name"].ToString();
				byte[] img = (byte[])record["image"];
				//convert binary to image
				MemoryStream ms = new MemoryStream(img);
				ovalPictureBox1.Image = System.Drawing.Image.FromStream(ms);
			}

			this.tea_regno = reg_no;
		}
		private void btn_resize_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Normal;
			btn_resize.Visible = false;
			btn_maximize.Visible = true;

		}

		private void btn_minimized_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		private void btn_maximize_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Maximized;
			btn_maximize.Visible = false;
			btn_resize.Visible = true;
		}

		private void btn_cancel_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		

		[DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
		private extern static void ReleaseCapture();
		[DllImport("user32.DLL", EntryPoint = "SendMessage")]

		private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

		private void pnl_barTop_MouseDown(object sender, MouseEventArgs e)
		{
			ReleaseCapture();
			SendMessage(this.Handle, 0x112, 0xf012, 0);
		}

		private void btn_off_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void toContainer(object ob)
		{
			if (this.pnl_container.Controls.Count > 0)
				this.pnl_container.Controls.RemoveAt(0);

			Form fh = ob as Form;
			fh.TopLevel = false;
			fh.Dock = DockStyle.Fill;
			this.pnl_container.Controls.Add(fh);
			this.pnl_container.Tag = fh;
			fh.Show();
		}

		private void btn_dashboard_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_dashboard(tea_regno));
			panel1.Show();
			panel2.Hide();
			panel3.Hide();
			panel4.Hide();
			panel5.Hide();
			panel6.Hide();
		}

		private void btn_stu_rec_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_stu_record());
			panel1.Hide();
			panel2.Show();
			panel3.Hide();
			panel4.Hide();
			panel5.Hide();
			panel6.Hide();
		}

		private void btn_Enter_resul_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_res());
			panel1.Hide();
			panel2.Hide();
			panel3.Show();
			panel4.Hide();
			panel5.Hide();
			panel6.Hide();
		}

		private void btn_update_marks_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_edit_stu_marks());
			panel1.Hide();
			panel2.Hide();
			panel3.Hide();
			panel4.Show();
			panel5.Hide();
			panel6.Hide();
		}

		private void btn_email_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_email(tea_regno));
			panel1.Hide();
			panel2.Hide();
			panel3.Hide();
			panel4.Hide();
			panel5.Show();
			panel6.Hide();
		}

		private void btn_teacher_ingo_Click(object sender, EventArgs e)
		{
			toContainer(new Teacher_info(tea_regno));
			panel1.Hide();
			panel2.Hide();
			panel3.Hide();
			panel4.Hide();
			panel5.Hide();
			panel6.Show();
		}
	}
}
