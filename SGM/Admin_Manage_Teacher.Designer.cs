﻿namespace SGM
{
	partial class Admin_Manage_Teacher
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.lable1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.txtReg_no = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.txtAge = new System.Windows.Forms.TextBox();
			this.txtContact_no = new System.Windows.Forms.TextBox();
			this.txtMail = new System.Windows.Forms.TextBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.dateTimePicker1_leavedate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker2_enterdate = new System.Windows.Forms.DateTimePicker();
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.btnupdate = new System.Windows.Forms.Button();
			this.btnDisplay = new System.Windows.Forms.Button();
			this.lvDis = new System.Windows.Forms.ListView();
			this.Id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.reg_no = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tea_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.age = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.sex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.address = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.contact_no = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.enter_date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.leave_date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.email = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.button1 = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button2 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Manage Teacher";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// btnSubmit
			// 
			this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSubmit.ForeColor = System.Drawing.Color.Black;
			this.btnSubmit.Location = new System.Drawing.Point(344, 449);
			this.btnSubmit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(100, 28);
			this.btnSubmit.TabIndex = 1;
			this.btnSubmit.Text = "Submit";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// lable1
			// 
			this.lable1.AutoSize = true;
			this.lable1.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.lable1.Location = new System.Drawing.Point(25, 60);
			this.lable1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lable1.Name = "lable1";
			this.lable1.Size = new System.Drawing.Size(83, 17);
			this.lable1.TabIndex = 2;
			this.lable1.Text = "Register No";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label3.Location = new System.Drawing.Point(25, 102);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(45, 17);
			this.label3.TabIndex = 3;
			this.label3.Text = "Name";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label4.Location = new System.Drawing.Point(25, 137);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(33, 17);
			this.label4.TabIndex = 4;
			this.label4.Text = "Age";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label5.Location = new System.Drawing.Point(27, 176);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(31, 17);
			this.label5.TabIndex = 5;
			this.label5.Text = "Sex";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label6.Location = new System.Drawing.Point(25, 226);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(60, 17);
			this.label6.TabIndex = 6;
			this.label6.Text = "Address";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label7.Location = new System.Drawing.Point(25, 263);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(78, 17);
			this.label7.TabIndex = 7;
			this.label7.Text = "Contact No";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label8.Location = new System.Drawing.Point(31, 334);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(45, 17);
			this.label8.TabIndex = 8;
			this.label8.Text = "Photo";
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label9.AutoSize = true;
			this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label9.Location = new System.Drawing.Point(27, 409);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(91, 17);
			this.label9.TabIndex = 9;
			this.label9.Text = "Date of enter";
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label10.AutoSize = true;
			this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label10.Location = new System.Drawing.Point(25, 449);
			this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(97, 17);
			this.label10.TabIndex = 10;
			this.label10.Text = "Date of Leave";
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label11.AutoSize = true;
			this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label11.Location = new System.Drawing.Point(29, 486);
			this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(42, 17);
			this.label11.TabIndex = 11;
			this.label11.Text = "Email";
			// 
			// txtReg_no
			// 
			this.txtReg_no.Location = new System.Drawing.Point(164, 52);
			this.txtReg_no.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtReg_no.Name = "txtReg_no";
			this.txtReg_no.Size = new System.Drawing.Size(132, 22);
			this.txtReg_no.TabIndex = 12;
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(164, 94);
			this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(132, 22);
			this.txtName.TabIndex = 13;
			// 
			// txtAge
			// 
			this.txtAge.Location = new System.Drawing.Point(164, 137);
			this.txtAge.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtAge.Name = "txtAge";
			this.txtAge.Size = new System.Drawing.Size(132, 22);
			this.txtAge.TabIndex = 14;
			// 
			// txtContact_no
			// 
			this.txtContact_no.Location = new System.Drawing.Point(164, 255);
			this.txtContact_no.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtContact_no.Name = "txtContact_no";
			this.txtContact_no.Size = new System.Drawing.Size(132, 22);
			this.txtContact_no.TabIndex = 15;
			// 
			// txtMail
			// 
			this.txtMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtMail.Location = new System.Drawing.Point(164, 478);
			this.txtMail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtMail.Name = "txtMail";
			this.txtMail.Size = new System.Drawing.Size(132, 22);
			this.txtMail.TabIndex = 16;
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
            "Female",
            "Male"});
			this.comboBox1.Location = new System.Drawing.Point(164, 176);
			this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(132, 24);
			this.comboBox1.TabIndex = 17;
			this.comboBox1.Tag = "Female";
			// 
			// dateTimePicker1_leavedate
			// 
			this.dateTimePicker1_leavedate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dateTimePicker1_leavedate.CustomFormat = "yyyy-MM-dd";
			this.dateTimePicker1_leavedate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker1_leavedate.Location = new System.Drawing.Point(164, 441);
			this.dateTimePicker1_leavedate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.dateTimePicker1_leavedate.Name = "dateTimePicker1_leavedate";
			this.dateTimePicker1_leavedate.Size = new System.Drawing.Size(132, 22);
			this.dateTimePicker1_leavedate.TabIndex = 18;
			// 
			// dateTimePicker2_enterdate
			// 
			this.dateTimePicker2_enterdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dateTimePicker2_enterdate.CustomFormat = "yyyy-MM-dd";
			this.dateTimePicker2_enterdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker2_enterdate.Location = new System.Drawing.Point(164, 401);
			this.dateTimePicker2_enterdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.dateTimePicker2_enterdate.Name = "dateTimePicker2_enterdate";
			this.dateTimePicker2_enterdate.Size = new System.Drawing.Size(132, 22);
			this.dateTimePicker2_enterdate.TabIndex = 19;
			// 
			// txtAddress
			// 
			this.txtAddress.Location = new System.Drawing.Point(164, 217);
			this.txtAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Size = new System.Drawing.Size(132, 22);
			this.txtAddress.TabIndex = 20;
			// 
			// btnupdate
			// 
			this.btnupdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnupdate.BackColor = System.Drawing.Color.MintCream;
			this.btnupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnupdate.ForeColor = System.Drawing.Color.Black;
			this.btnupdate.Location = new System.Drawing.Point(476, 448);
			this.btnupdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnupdate.Name = "btnupdate";
			this.btnupdate.Size = new System.Drawing.Size(100, 28);
			this.btnupdate.TabIndex = 22;
			this.btnupdate.Text = "Update";
			this.btnupdate.UseVisualStyleBackColor = false;
			this.btnupdate.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnDisplay
			// 
			this.btnDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDisplay.ForeColor = System.Drawing.Color.Black;
			this.btnDisplay.Location = new System.Drawing.Point(719, 448);
			this.btnDisplay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnDisplay.Name = "btnDisplay";
			this.btnDisplay.Size = new System.Drawing.Size(100, 28);
			this.btnDisplay.TabIndex = 23;
			this.btnDisplay.Text = "Display";
			this.btnDisplay.UseVisualStyleBackColor = true;
			this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
			// 
			// lvDis
			// 
			this.lvDis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lvDis.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Id,
            this.reg_no,
            this.tea_name,
            this.age,
            this.sex,
            this.address,
            this.contact_no,
            this.enter_date,
            this.leave_date,
            this.email});
			this.lvDis.FullRowSelect = true;
			this.lvDis.Location = new System.Drawing.Point(320, 52);
			this.lvDis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.lvDis.Name = "lvDis";
			this.lvDis.Size = new System.Drawing.Size(497, 227);
			this.lvDis.TabIndex = 24;
			this.lvDis.UseCompatibleStateImageBehavior = false;
			this.lvDis.View = System.Windows.Forms.View.Details;
			this.lvDis.SelectedIndexChanged += new System.EventHandler(this.lvDis_SelectedIndexChanged);
			// 
			// Id
			// 
			this.Id.Text = "ID";
			this.Id.Width = 80;
			// 
			// reg_no
			// 
			this.reg_no.Text = "Register No";
			this.reg_no.Width = 90;
			// 
			// tea_name
			// 
			this.tea_name.Text = "Name";
			this.tea_name.Width = 90;
			// 
			// age
			// 
			this.age.Text = "Age";
			// 
			// sex
			// 
			this.sex.Text = "Sex";
			// 
			// address
			// 
			this.address.Text = "Address";
			this.address.Width = 100;
			// 
			// contact_no
			// 
			this.contact_no.Text = "Contact No";
			this.contact_no.Width = 80;
			// 
			// enter_date
			// 
			this.enter_date.Text = "Date of enter";
			this.enter_date.Width = 90;
			// 
			// leave_date
			// 
			this.leave_date.Text = "Date of leave";
			this.leave_date.Width = 90;
			// 
			// email
			// 
			this.email.Text = "Email";
			this.email.Width = 100;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.Black;
			this.button1.Location = new System.Drawing.Point(607, 448);
			this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 28);
			this.button1.TabIndex = 25;
			this.button1.Text = "Delete";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Location = new System.Drawing.Point(164, 304);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(132, 89);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 26;
			this.pictureBox1.TabStop = false;
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.Color.Black;
			this.button2.Location = new System.Drawing.Point(304, 395);
			this.button2.Margin = new System.Windows.Forms.Padding(4);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(100, 28);
			this.button2.TabIndex = 1;
			this.button2.Text = "Browse";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// Admin_Manage_Teacher
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lvDis);
			this.Controls.Add(this.btnDisplay);
			this.Controls.Add(this.btnupdate);
			this.Controls.Add(this.txtAddress);
			this.Controls.Add(this.dateTimePicker2_enterdate);
			this.Controls.Add(this.dateTimePicker1_leavedate);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.txtMail);
			this.Controls.Add(this.txtContact_no);
			this.Controls.Add(this.txtAge);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.txtReg_no);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lable1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.btnSubmit);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "Admin_Manage_Teacher";
			this.Text = "Admin_Manage_Teacher";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lable1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtReg_no;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.TextBox txtContact_no;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1_leavedate;
        private System.Windows.Forms.DateTimePicker dateTimePicker2_enterdate;
        private System.Windows.Forms.TextBox txtAddress;
       
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.ListView lvDis;
        private System.Windows.Forms.ColumnHeader Id;
        private System.Windows.Forms.ColumnHeader reg_no;
        private System.Windows.Forms.ColumnHeader tea_name;
        private System.Windows.Forms.ColumnHeader age;
        private System.Windows.Forms.ColumnHeader sex;
        private System.Windows.Forms.ColumnHeader address;
        private System.Windows.Forms.ColumnHeader contact_no;
        private System.Windows.Forms.ColumnHeader enter_date;
        private System.Windows.Forms.ColumnHeader leave_date;
        private System.Windows.Forms.ColumnHeader email;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button2;
	}
}