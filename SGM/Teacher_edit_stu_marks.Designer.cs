﻿namespace SGM
{
	partial class Teacher_edit_stu_marks
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Teacher_edit_stu_marks));
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.txt_math = new System.Windows.Forms.RichTextBox();
			this.txt_science = new System.Windows.Forms.RichTextBox();
			this.txt_sinhala = new System.Windows.Forms.RichTextBox();
			this.txt_religion = new System.Windows.Forms.RichTextBox();
			this.txt_english = new System.Windows.Forms.RichTextBox();
			this.txt_music = new System.Windows.Forms.RichTextBox();
			this.txt_dance = new System.Windows.Forms.RichTextBox();
			this.txt_commerce = new System.Windows.Forms.RichTextBox();
			this.txt_it = new System.Windows.Forms.RichTextBox();
			this.txt_elit = new System.Windows.Forms.RichTextBox();
			this.txt_health = new System.Windows.Forms.RichTextBox();
			this.txt_geo = new System.Windows.Forms.RichTextBox();
			this.txt_history = new System.Windows.Forms.RichTextBox();
			this.txt_media = new System.Windows.Forms.RichTextBox();
			this.txt_slit = new System.Windows.Forms.RichTextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.txt_art = new System.Windows.Forms.RichTextBox();
			this.txt_dramma = new System.Windows.Forms.RichTextBox();
			this.txt_tamil = new System.Windows.Forms.RichTextBox();
			this.btn_edit = new System.Windows.Forms.Button();
			this.btn_update = new System.Windows.Forms.Button();
			this.btn_search = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Edit Student Marks";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 8;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.90476F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.90476F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.42857F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.90476F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tableLayoutPanel1.Controls.Add(this.label2, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label3, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label4, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.label5, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.label6, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.label7, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.label8, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.label9, 3, 3);
			this.tableLayoutPanel1.Controls.Add(this.label10, 3, 4);
			this.tableLayoutPanel1.Controls.Add(this.label11, 3, 5);
			this.tableLayoutPanel1.Controls.Add(this.label12, 5, 1);
			this.tableLayoutPanel1.Controls.Add(this.label13, 5, 2);
			this.tableLayoutPanel1.Controls.Add(this.label14, 5, 3);
			this.tableLayoutPanel1.Controls.Add(this.label15, 5, 4);
			this.tableLayoutPanel1.Controls.Add(this.label16, 5, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_math, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_science, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.txt_sinhala, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.txt_religion, 2, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_english, 2, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_music, 4, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_dance, 4, 2);
			this.tableLayoutPanel1.Controls.Add(this.txt_commerce, 4, 3);
			this.tableLayoutPanel1.Controls.Add(this.txt_it, 4, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_elit, 4, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_health, 6, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_geo, 6, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_history, 6, 3);
			this.tableLayoutPanel1.Controls.Add(this.txt_media, 6, 2);
			this.tableLayoutPanel1.Controls.Add(this.txt_slit, 6, 1);
			this.tableLayoutPanel1.Controls.Add(this.label17, 1, 6);
			this.tableLayoutPanel1.Controls.Add(this.label18, 3, 6);
			this.tableLayoutPanel1.Controls.Add(this.label19, 5, 6);
			this.tableLayoutPanel1.Controls.Add(this.txt_art, 2, 6);
			this.tableLayoutPanel1.Controls.Add(this.txt_dramma, 4, 6);
			this.tableLayoutPanel1.Controls.Add(this.txt_tamil, 6, 6);
			this.tableLayoutPanel1.Controls.Add(this.btn_edit, 2, 8);
			this.tableLayoutPanel1.Controls.Add(this.btn_update, 5, 8);
			this.tableLayoutPanel1.Controls.Add(this.btn_search, 1, 9);
			this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 7, 8);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 11;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(835, 487);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label2.Location = new System.Drawing.Point(23, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(160, 50);
			this.label2.TabIndex = 0;
			this.label2.Text = "Maths";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label3.Location = new System.Drawing.Point(23, 70);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(160, 50);
			this.label3.TabIndex = 0;
			this.label3.Text = "Science";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label4.Location = new System.Drawing.Point(23, 120);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(160, 50);
			this.label4.TabIndex = 0;
			this.label4.Text = "Sinhala";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label5.Location = new System.Drawing.Point(23, 170);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(160, 50);
			this.label5.TabIndex = 0;
			this.label5.Text = "Religion";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label6.Location = new System.Drawing.Point(23, 220);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(160, 50);
			this.label6.TabIndex = 0;
			this.label6.Text = "English";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label7.Location = new System.Drawing.Point(281, 20);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(160, 50);
			this.label7.TabIndex = 0;
			this.label7.Text = "Music";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label8.Location = new System.Drawing.Point(281, 70);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(160, 50);
			this.label8.TabIndex = 0;
			this.label8.Text = "Dancing";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label9.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label9.Location = new System.Drawing.Point(281, 120);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(160, 50);
			this.label9.TabIndex = 0;
			this.label9.Text = "Commerce";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label10.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label10.Location = new System.Drawing.Point(281, 170);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(160, 50);
			this.label10.TabIndex = 0;
			this.label10.Text = "IT";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label11.Location = new System.Drawing.Point(281, 220);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(160, 50);
			this.label11.TabIndex = 0;
			this.label11.Text = "E.Lit";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label12.Location = new System.Drawing.Point(539, 20);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(160, 50);
			this.label12.TabIndex = 0;
			this.label12.Text = "S.Lit";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label13.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label13.Location = new System.Drawing.Point(539, 70);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(160, 50);
			this.label13.TabIndex = 0;
			this.label13.Text = "Media";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label14.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label14.Location = new System.Drawing.Point(539, 120);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(160, 50);
			this.label14.TabIndex = 0;
			this.label14.Text = "History";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label15.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label15.Location = new System.Drawing.Point(539, 170);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(160, 50);
			this.label15.TabIndex = 0;
			this.label15.Text = "Health Science";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label16.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label16.Location = new System.Drawing.Point(539, 220);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(160, 50);
			this.label16.TabIndex = 0;
			this.label16.Text = "Geography";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txt_math
			// 
			this.txt_math.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_math.Location = new System.Drawing.Point(189, 23);
			this.txt_math.Name = "txt_math";
			this.txt_math.Size = new System.Drawing.Size(86, 44);
			this.txt_math.TabIndex = 1;
			this.txt_math.Text = "";
			// 
			// txt_science
			// 
			this.txt_science.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_science.Location = new System.Drawing.Point(189, 73);
			this.txt_science.Name = "txt_science";
			this.txt_science.Size = new System.Drawing.Size(86, 44);
			this.txt_science.TabIndex = 1;
			this.txt_science.Text = "";
			// 
			// txt_sinhala
			// 
			this.txt_sinhala.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_sinhala.Location = new System.Drawing.Point(189, 123);
			this.txt_sinhala.Name = "txt_sinhala";
			this.txt_sinhala.Size = new System.Drawing.Size(86, 44);
			this.txt_sinhala.TabIndex = 1;
			this.txt_sinhala.Text = "";
			// 
			// txt_religion
			// 
			this.txt_religion.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_religion.Location = new System.Drawing.Point(189, 173);
			this.txt_religion.Name = "txt_religion";
			this.txt_religion.Size = new System.Drawing.Size(86, 44);
			this.txt_religion.TabIndex = 1;
			this.txt_religion.Text = "";
			// 
			// txt_english
			// 
			this.txt_english.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_english.Location = new System.Drawing.Point(189, 223);
			this.txt_english.Name = "txt_english";
			this.txt_english.Size = new System.Drawing.Size(86, 44);
			this.txt_english.TabIndex = 1;
			this.txt_english.Text = "";
			// 
			// txt_music
			// 
			this.txt_music.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_music.Location = new System.Drawing.Point(447, 23);
			this.txt_music.Name = "txt_music";
			this.txt_music.Size = new System.Drawing.Size(86, 44);
			this.txt_music.TabIndex = 1;
			this.txt_music.Text = "";
			// 
			// txt_dance
			// 
			this.txt_dance.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_dance.Location = new System.Drawing.Point(447, 73);
			this.txt_dance.Name = "txt_dance";
			this.txt_dance.Size = new System.Drawing.Size(86, 44);
			this.txt_dance.TabIndex = 1;
			this.txt_dance.Text = "";
			// 
			// txt_commerce
			// 
			this.txt_commerce.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_commerce.Location = new System.Drawing.Point(447, 123);
			this.txt_commerce.Name = "txt_commerce";
			this.txt_commerce.Size = new System.Drawing.Size(86, 44);
			this.txt_commerce.TabIndex = 1;
			this.txt_commerce.Text = "";
			// 
			// txt_it
			// 
			this.txt_it.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_it.Location = new System.Drawing.Point(447, 173);
			this.txt_it.Name = "txt_it";
			this.txt_it.Size = new System.Drawing.Size(86, 44);
			this.txt_it.TabIndex = 1;
			this.txt_it.Text = "";
			// 
			// txt_elit
			// 
			this.txt_elit.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_elit.Location = new System.Drawing.Point(447, 223);
			this.txt_elit.Name = "txt_elit";
			this.txt_elit.Size = new System.Drawing.Size(86, 44);
			this.txt_elit.TabIndex = 1;
			this.txt_elit.Text = "";
			// 
			// txt_health
			// 
			this.txt_health.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_health.Location = new System.Drawing.Point(705, 173);
			this.txt_health.Name = "txt_health";
			this.txt_health.Size = new System.Drawing.Size(86, 44);
			this.txt_health.TabIndex = 1;
			this.txt_health.Text = "";
			// 
			// txt_geo
			// 
			this.txt_geo.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_geo.Location = new System.Drawing.Point(705, 223);
			this.txt_geo.Name = "txt_geo";
			this.txt_geo.Size = new System.Drawing.Size(86, 44);
			this.txt_geo.TabIndex = 1;
			this.txt_geo.Text = "";
			// 
			// txt_history
			// 
			this.txt_history.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_history.Location = new System.Drawing.Point(705, 123);
			this.txt_history.Name = "txt_history";
			this.txt_history.Size = new System.Drawing.Size(86, 44);
			this.txt_history.TabIndex = 1;
			this.txt_history.Text = "";
			// 
			// txt_media
			// 
			this.txt_media.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_media.Location = new System.Drawing.Point(705, 73);
			this.txt_media.Name = "txt_media";
			this.txt_media.Size = new System.Drawing.Size(86, 44);
			this.txt_media.TabIndex = 1;
			this.txt_media.Text = "";
			// 
			// txt_slit
			// 
			this.txt_slit.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_slit.Location = new System.Drawing.Point(705, 23);
			this.txt_slit.Name = "txt_slit";
			this.txt_slit.Size = new System.Drawing.Size(86, 44);
			this.txt_slit.TabIndex = 1;
			this.txt_slit.Text = "";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label17.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label17.Location = new System.Drawing.Point(23, 270);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(160, 50);
			this.label17.TabIndex = 0;
			this.label17.Text = "Art";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label18.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label18.Location = new System.Drawing.Point(281, 270);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(160, 50);
			this.label18.TabIndex = 0;
			this.label18.Text = "Dramma";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label19.Font = new System.Drawing.Font("Open Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label19.Location = new System.Drawing.Point(539, 270);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(160, 50);
			this.label19.TabIndex = 0;
			this.label19.Text = "Tamil";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txt_art
			// 
			this.txt_art.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_art.Location = new System.Drawing.Point(189, 273);
			this.txt_art.Name = "txt_art";
			this.txt_art.Size = new System.Drawing.Size(86, 44);
			this.txt_art.TabIndex = 1;
			this.txt_art.Text = "";
			// 
			// txt_dramma
			// 
			this.txt_dramma.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_dramma.Location = new System.Drawing.Point(447, 273);
			this.txt_dramma.Name = "txt_dramma";
			this.txt_dramma.Size = new System.Drawing.Size(86, 44);
			this.txt_dramma.TabIndex = 1;
			this.txt_dramma.Text = "";
			// 
			// txt_tamil
			// 
			this.txt_tamil.Font = new System.Drawing.Font("Open Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_tamil.Location = new System.Drawing.Point(705, 273);
			this.txt_tamil.Name = "txt_tamil";
			this.txt_tamil.Size = new System.Drawing.Size(86, 44);
			this.txt_tamil.TabIndex = 1;
			this.txt_tamil.Text = "";
			// 
			// btn_edit
			// 
			this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.tableLayoutPanel1.SetColumnSpan(this.btn_edit, 2);
			this.btn_edit.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_edit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_edit.Location = new System.Drawing.Point(189, 373);
			this.btn_edit.Name = "btn_edit";
			this.btn_edit.Size = new System.Drawing.Size(237, 43);
			this.btn_edit.TabIndex = 2;
			this.btn_edit.Text = "Edit";
			this.btn_edit.UseVisualStyleBackColor = false;
			this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
			// 
			// btn_update
			// 
			this.btn_update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_update.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btn_update.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_update.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_update.Location = new System.Drawing.Point(539, 373);
			this.btn_update.Name = "btn_update";
			this.btn_update.Size = new System.Drawing.Size(160, 44);
			this.btn_update.TabIndex = 2;
			this.btn_update.Text = "Update";
			this.btn_update.UseVisualStyleBackColor = false;
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click_1);
			// 
			// btn_search
			// 
			this.btn_search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.tableLayoutPanel1.SetColumnSpan(this.btn_search, 6);
			this.btn_search.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_search.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_search.Location = new System.Drawing.Point(23, 423);
			this.btn_search.Name = "btn_search";
			this.btn_search.Size = new System.Drawing.Size(768, 44);
			this.btn_search.TabIndex = 2;
			this.btn_search.Text = "Seach Student";
			this.btn_search.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btn_search.UseVisualStyleBackColor = false;
			this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(797, 373);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(35, 44);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// Teacher_edit_stu_marks
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Teacher_edit_stu_marks";
			this.Text = "Teacher_edit_stu_marks";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.RichTextBox txt_math;
		private System.Windows.Forms.RichTextBox txt_science;
		private System.Windows.Forms.RichTextBox txt_sinhala;
		private System.Windows.Forms.RichTextBox txt_religion;
		private System.Windows.Forms.RichTextBox txt_english;
		private System.Windows.Forms.RichTextBox txt_music;
		private System.Windows.Forms.RichTextBox txt_dance;
		private System.Windows.Forms.RichTextBox txt_commerce;
		private System.Windows.Forms.RichTextBox txt_it;
		private System.Windows.Forms.RichTextBox txt_elit;
		private System.Windows.Forms.RichTextBox txt_health;
		private System.Windows.Forms.RichTextBox txt_geo;
		private System.Windows.Forms.RichTextBox txt_history;
		private System.Windows.Forms.RichTextBox txt_media;
		private System.Windows.Forms.RichTextBox txt_slit;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.RichTextBox txt_art;
		private System.Windows.Forms.RichTextBox txt_dramma;
		private System.Windows.Forms.RichTextBox txt_tamil;
		private System.Windows.Forms.Button btn_edit;
		private System.Windows.Forms.Button btn_update;
		private System.Windows.Forms.Button btn_search;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}