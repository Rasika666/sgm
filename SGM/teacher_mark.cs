﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using SGM.database;
using System.IO;
using SGM.models;

namespace SGM
{
	public partial class teacher_mark : Form
	{
		private String selected_regNo;
		
		public teacher_mark()
		{
			InitializeComponent();
			DB.initializedDB();
			load_grade();
		}

		
		public void setReg_no(String r)
		{
			this.selected_regNo = r;
		}

		public String getReg_no()
		{
			return this.selected_regNo;
		}

		private void btn_resize_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Normal;
			btn_resize.Visible = false;
			btn_maximize.Visible = true;

		}

		private void btn_minimized_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		private void btn_maximize_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Maximized;
			btn_maximize.Visible = false;
			btn_resize.Visible = true;
		}

		private void btn_cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		

		[DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
		private extern static void ReleaseCapture();
		[DllImport("user32.DLL", EntryPoint = "SendMessage")]

		private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

		private void pnl_barTop_MouseDown(object sender, MouseEventArgs e)
		{
			ReleaseCapture();
			SendMessage(this.Handle, 0x112, 0xf012, 0);
		}

		private void btn_off_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var query = new StringBuilder();
			String sql = "SELECT m.grade, m.sem, s.*,e.email " +
				"FROM student_info s, email e,  marks m " +
				"WHERE s.reg_no=e.reg_no AND s.reg_no=m.reg_no";
			query.Append(sql);
			if (!String.IsNullOrEmpty(combo_grade.Text))
			{
				query.Append(" AND m.grade = '" + combo_grade.Text + "'");
			}
			if (!String.IsNullOrEmpty(combo_class.Text))
			{
				query.Append(" AND m.sem = '" + combo_class.Text + "'");
			}
			if (!String.IsNullOrEmpty(combo_name.Text))
			{
				query.Append(" AND s.name_with_initials LIKE '%" + combo_name.Text + "%' OR s.full_name LIKE '%" + combo_name.Text + "%'");
			}
			loadAll(query.ToString());

		}

		private void loadAll(String s)
		{
			List<Current_Grade_info_model> stu = Current_Grade_info_model.GetStu(s);

			//clear the list view
			listView1.Items.Clear();

			foreach (Current_Grade_info_model u in stu)
			{
				ListViewItem item = new ListViewItem(new String[] { u.reg_no, u.name_with_initials,u.grade.ToString(),u.sem.ToString(),u.email});
				item.Tag = u;

				listView1.Items.Add(item);
			}
		}

		private void load_grade()
		{
			String sql = "SELECT  DISTINCT(grade) FROM current_stu_grade";

			var record = DB.getData(sql);

			while (record.Read())
			{
				String grade = record["grade"].ToString();
				combo_grade.Items.Add(grade);
			}
		}

		public void load_name()
		{
			/***********************************************************************
			 * 
			 * Dynamic building query
			 * *******************************************************
			     	var query = new StringBuilder();
					query.Append("select ... From ... Where 1=1");
					if(fooHasValue) {
						query.Append(" and Foo = @foo");
						cmd.Parameters.AddWithValue("foo", foo);
					}
					if(barHasValue) {
						query.Append(" and Bar = @bar");
						cmd.Parameters.AddWithValue("bar", bar);
					}
					cmd.CommandText = query.ToString();
		
			 * **********************************************************************/
			var query = new StringBuilder();
			String sql = "SELECT s.name_with_initials FROM student_info s, marks m WHERE s.reg_no=m.reg_no";
			query.Append(sql);
			if (!String.IsNullOrEmpty(combo_grade.Text))
			{
				query.Append(" AND c.grade = '"+ combo_grade.Text + "'");
			}
			if (!String.IsNullOrEmpty(combo_class.Text))
			{
				query.Append(" AND m.sem = '" + combo_class.Text + "'");
			}

			var record = DB.getData(query.ToString());
			while(record.Read())
			{
				String name = record["name_with_initials"].ToString();
				combo_name.Items.Add(name);
			}
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			load_name();
		}

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count > 0)
			{
				ListViewItem item = listView1.SelectedItems[0];

				setReg_no(item.SubItems[0].Text);
			}

			Teacher_edit_stu_marks.regno = getReg_no();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
