﻿namespace SGM
{
	partial class layout_stu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(layout_stu));
			this.pnl_barTop = new System.Windows.Forms.Panel();
			this.pic_avatar = new OvalPictureBox();
			this.lbl_name = new System.Windows.Forms.Label();
			this.btn_minimized = new System.Windows.Forms.PictureBox();
			this.btn_maximize = new System.Windows.Forms.PictureBox();
			this.btn_cancel_panel = new System.Windows.Forms.PictureBox();
			this.btn_resize = new System.Windows.Forms.PictureBox();
			this.pnl_menuBar = new System.Windows.Forms.Panel();
			this.btn_off = new System.Windows.Forms.PictureBox();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btn_student_info = new System.Windows.Forms.Button();
			this.btn_progress = new System.Windows.Forms.Button();
			this.btn_result = new System.Windows.Forms.Button();
			this.btn_dashboard = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pnl_container = new System.Windows.Forms.Panel();
			this.pnl_barTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avatar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).BeginInit();
			this.pnl_menuBar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_off)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_barTop
			// 
			this.pnl_barTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.pnl_barTop.Controls.Add(this.pic_avatar);
			this.pnl_barTop.Controls.Add(this.lbl_name);
			this.pnl_barTop.Controls.Add(this.btn_minimized);
			this.pnl_barTop.Controls.Add(this.btn_maximize);
			this.pnl_barTop.Controls.Add(this.btn_cancel_panel);
			this.pnl_barTop.Controls.Add(this.btn_resize);
			this.pnl_barTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnl_barTop.Location = new System.Drawing.Point(0, 0);
			this.pnl_barTop.Name = "pnl_barTop";
			this.pnl_barTop.Size = new System.Drawing.Size(1300, 40);
			this.pnl_barTop.TabIndex = 0;
			this.pnl_barTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_barTop_MouseDown);
			// 
			// pic_avatar
			// 
			this.pic_avatar.BackColor = System.Drawing.Color.DarkGray;
			this.pic_avatar.Image = ((System.Drawing.Image)(resources.GetObject("pic_avatar.Image")));
			this.pic_avatar.Location = new System.Drawing.Point(33, 0);
			this.pic_avatar.Name = "pic_avatar";
			this.pic_avatar.Size = new System.Drawing.Size(40, 40);
			this.pic_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pic_avatar.TabIndex = 3;
			this.pic_avatar.TabStop = false;
			// 
			// lbl_name
			// 
			this.lbl_name.AutoSize = true;
			this.lbl_name.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_name.ForeColor = System.Drawing.Color.White;
			this.lbl_name.Location = new System.Drawing.Point(97, 7);
			this.lbl_name.Name = "lbl_name";
			this.lbl_name.Size = new System.Drawing.Size(70, 27);
			this.lbl_name.TabIndex = 2;
			this.lbl_name.Text = "avatar";
			// 
			// btn_minimized
			// 
			this.btn_minimized.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_minimized.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_minimized.Image = global::SGM.Properties.Resources.minus;
			this.btn_minimized.Location = new System.Drawing.Point(1178, 9);
			this.btn_minimized.Name = "btn_minimized";
			this.btn_minimized.Size = new System.Drawing.Size(25, 25);
			this.btn_minimized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_minimized.TabIndex = 0;
			this.btn_minimized.TabStop = false;
			this.btn_minimized.Click += new System.EventHandler(this.btn_minimized_Click);
			// 
			// btn_maximize
			// 
			this.btn_maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_maximize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_maximize.Image = global::SGM.Properties.Resources.maximize;
			this.btn_maximize.Location = new System.Drawing.Point(1220, 9);
			this.btn_maximize.Name = "btn_maximize";
			this.btn_maximize.Size = new System.Drawing.Size(25, 25);
			this.btn_maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_maximize.TabIndex = 0;
			this.btn_maximize.TabStop = false;
			this.btn_maximize.Click += new System.EventHandler(this.btn_maximize_Click);
			// 
			// btn_cancel_panel
			// 
			this.btn_cancel_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_cancel_panel.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_cancel_panel.Image = global::SGM.Properties.Resources.cancel;
			this.btn_cancel_panel.Location = new System.Drawing.Point(1263, 9);
			this.btn_cancel_panel.Name = "btn_cancel_panel";
			this.btn_cancel_panel.Size = new System.Drawing.Size(25, 25);
			this.btn_cancel_panel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_cancel_panel.TabIndex = 0;
			this.btn_cancel_panel.TabStop = false;
			this.btn_cancel_panel.Click += new System.EventHandler(this.btn_cancel_Click);
			// 
			// btn_resize
			// 
			this.btn_resize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_resize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_resize.Image = global::SGM.Properties.Resources.scale;
			this.btn_resize.Location = new System.Drawing.Point(1220, 9);
			this.btn_resize.Name = "btn_resize";
			this.btn_resize.Size = new System.Drawing.Size(25, 25);
			this.btn_resize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_resize.TabIndex = 0;
			this.btn_resize.TabStop = false;
			this.btn_resize.Click += new System.EventHandler(this.btn_resize_Click);
			// 
			// pnl_menuBar
			// 
			this.pnl_menuBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.pnl_menuBar.Controls.Add(this.btn_off);
			this.pnl_menuBar.Controls.Add(this.panel4);
			this.pnl_menuBar.Controls.Add(this.panel3);
			this.pnl_menuBar.Controls.Add(this.panel2);
			this.pnl_menuBar.Controls.Add(this.panel1);
			this.pnl_menuBar.Controls.Add(this.btn_student_info);
			this.pnl_menuBar.Controls.Add(this.btn_progress);
			this.pnl_menuBar.Controls.Add(this.btn_result);
			this.pnl_menuBar.Controls.Add(this.btn_dashboard);
			this.pnl_menuBar.Controls.Add(this.pictureBox1);
			this.pnl_menuBar.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnl_menuBar.Location = new System.Drawing.Point(0, 40);
			this.pnl_menuBar.Name = "pnl_menuBar";
			this.pnl_menuBar.Size = new System.Drawing.Size(280, 610);
			this.pnl_menuBar.TabIndex = 1;
			// 
			// btn_off
			// 
			this.btn_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btn_off.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_off.Image = ((System.Drawing.Image)(resources.GetObject("btn_off.Image")));
			this.btn_off.Location = new System.Drawing.Point(12, 566);
			this.btn_off.Name = "btn_off";
			this.btn_off.Size = new System.Drawing.Size(32, 32);
			this.btn_off.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.btn_off.TabIndex = 3;
			this.btn_off.TabStop = false;
			this.btn_off.Click += new System.EventHandler(this.btn_off_Click);
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel4.Location = new System.Drawing.Point(1, 323);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(10, 46);
			this.panel4.TabIndex = 2;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel3.Location = new System.Drawing.Point(1, 271);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(10, 46);
			this.panel3.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel2.Location = new System.Drawing.Point(1, 219);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(10, 46);
			this.panel2.TabIndex = 2;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel1.Location = new System.Drawing.Point(1, 167);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(10, 46);
			this.panel1.TabIndex = 2;
			// 
			// btn_student_info
			// 
			this.btn_student_info.FlatAppearance.BorderSize = 0;
			this.btn_student_info.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_student_info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_student_info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_student_info.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_student_info.ForeColor = System.Drawing.Color.White;
			this.btn_student_info.Image = ((System.Drawing.Image)(resources.GetObject("btn_student_info.Image")));
			this.btn_student_info.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_student_info.Location = new System.Drawing.Point(12, 323);
			this.btn_student_info.Name = "btn_student_info";
			this.btn_student_info.Size = new System.Drawing.Size(268, 46);
			this.btn_student_info.TabIndex = 1;
			this.btn_student_info.Text = "Student Info";
			this.btn_student_info.UseVisualStyleBackColor = true;
			this.btn_student_info.Click += new System.EventHandler(this.btn_student_info_Click);
			// 
			// btn_progress
			// 
			this.btn_progress.FlatAppearance.BorderSize = 0;
			this.btn_progress.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_progress.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_progress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_progress.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_progress.ForeColor = System.Drawing.Color.White;
			this.btn_progress.Image = ((System.Drawing.Image)(resources.GetObject("btn_progress.Image")));
			this.btn_progress.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_progress.Location = new System.Drawing.Point(12, 271);
			this.btn_progress.Name = "btn_progress";
			this.btn_progress.Size = new System.Drawing.Size(268, 46);
			this.btn_progress.TabIndex = 1;
			this.btn_progress.Text = "Progress";
			this.btn_progress.UseVisualStyleBackColor = true;
			this.btn_progress.Click += new System.EventHandler(this.btn_progress_Click);
			// 
			// btn_result
			// 
			this.btn_result.FlatAppearance.BorderSize = 0;
			this.btn_result.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_result.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_result.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_result.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_result.ForeColor = System.Drawing.Color.White;
			this.btn_result.Image = ((System.Drawing.Image)(resources.GetObject("btn_result.Image")));
			this.btn_result.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_result.Location = new System.Drawing.Point(12, 219);
			this.btn_result.Name = "btn_result";
			this.btn_result.Size = new System.Drawing.Size(268, 46);
			this.btn_result.TabIndex = 1;
			this.btn_result.Text = "Result";
			this.btn_result.UseVisualStyleBackColor = true;
			this.btn_result.Click += new System.EventHandler(this.btn_result_Click);
			// 
			// btn_dashboard
			// 
			this.btn_dashboard.FlatAppearance.BorderSize = 0;
			this.btn_dashboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_dashboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_dashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_dashboard.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_dashboard.ForeColor = System.Drawing.Color.White;
			this.btn_dashboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_dashboard.Image")));
			this.btn_dashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_dashboard.Location = new System.Drawing.Point(12, 167);
			this.btn_dashboard.Name = "btn_dashboard";
			this.btn_dashboard.Size = new System.Drawing.Size(268, 46);
			this.btn_dashboard.TabIndex = 1;
			this.btn_dashboard.Text = "Dashboard";
			this.btn_dashboard.UseVisualStyleBackColor = true;
			this.btn_dashboard.Click += new System.EventHandler(this.btn_dashboard_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::SGM.Properties.Resources._94facad9_b53f_469f_baf6_3eea74757acc;
			this.pictureBox1.Location = new System.Drawing.Point(0, -19);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(240, 201);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// pnl_container
			// 
			this.pnl_container.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl_container.ForeColor = System.Drawing.Color.White;
			this.pnl_container.Location = new System.Drawing.Point(280, 40);
			this.pnl_container.Name = "pnl_container";
			this.pnl_container.Size = new System.Drawing.Size(1020, 610);
			this.pnl_container.TabIndex = 2;
			// 
			// layout_stu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1300, 650);
			this.Controls.Add(this.pnl_container);
			this.Controls.Add(this.pnl_menuBar);
			this.Controls.Add(this.pnl_barTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "layout_stu";
			this.Text = "Form1";
			this.pnl_barTop.ResumeLayout(false);
			this.pnl_barTop.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avatar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).EndInit();
			this.pnl_menuBar.ResumeLayout(false);
			this.pnl_menuBar.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_off)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_barTop;
		private System.Windows.Forms.Panel pnl_menuBar;
		private System.Windows.Forms.Panel pnl_container;
		private System.Windows.Forms.PictureBox btn_cancel_panel;
		private System.Windows.Forms.PictureBox btn_minimized;
		private System.Windows.Forms.PictureBox btn_maximize;
		private System.Windows.Forms.PictureBox btn_resize;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btn_dashboard;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btn_student_info;
		private System.Windows.Forms.Button btn_progress;
		private System.Windows.Forms.Button btn_result;
		private System.Windows.Forms.PictureBox btn_off;
		private System.Windows.Forms.Label lbl_name;
		private OvalPictureBox pic_avatar;
	}
}

