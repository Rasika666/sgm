﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using System.Windows.Forms.DataVisualization.Charting;

namespace SGM
{
	public partial class Stu_dash : Form
	{
		private static string reg_no;
		public Stu_dash(string regno)
		{
			reg_no = regno;
			InitializeComponent();
			DB.initializedDB();
			load_date(regno);
			getMarks(regno);

		}

		private void load_date(String regno)
		{
			string sql = "SELECT full_name from student_info WHERE reg_no='" + regno + "'";
			var rec = DB.getData(sql);
			while (rec.Read())
			{

				lbl_full_name.Text = rec["full_name"].ToString();
			}
			sql = "SELECT grade,class FROM current_stu_grade WHERE reg_no='" + regno + "'";
			rec = DB.getData(sql);
			while (rec.Read())
			{
				lbl_class.Text = "Grade : " +rec["grade"].ToString()+" | "+ "Class : "+ rec["class"];
			}
		}

		public void getMarks(String regno)
		{
			var chart = chartMaths.ChartAreas[0];
			chartMaths.ChartAreas[0].AxisX.LabelStyle.Angle = 45;
			chart.AxisX.IntervalType = DateTimeIntervalType.Number;

			chart.AxisX.LabelStyle.Format = "";
			chart.AxisY.LabelStyle.Format = "";
			chart.AxisY.LabelStyle.IsEndLabelVisible = true;

			chart.AxisX.Minimum = 1;
			//chart.AxisX.Maximum = 24;
			chart.AxisY.Minimum = 0;
			//chart.AxisY.Maximum = 100;
			chart.AxisX.Interval = 1;
			chart.AxisY.Interval = 5;

			chartMaths.Series.Add("maths");
			chartMaths.Series["maths"].ChartType = SeriesChartType.Line;
			// spline will smooth the line
			chartMaths.Series["maths"].Color = Color.Red;
			chartMaths.Series[0].IsVisibleInLegend = false;

			String sql = String.Format("SELECT maths as y, CONCAT('grade',grade,' ', 'term',sem) as x " +
										"FROM  marks " +
										"WHERE reg_no = '{0}'" +
										"ORDER 	BY	grade,sem", regno);
			var record = DB.getData(sql);

			while (record.Read())
			{
				chartMaths.Series["maths"].Points.AddXY(record["x"], record["y"]);
			}
		}


	}
}
