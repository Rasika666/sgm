﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace SGM
{
	public partial class Teacher_email : Form
	{

		public static String stu_reg_no;
		private String tea_reg_no;
		public Teacher_email(String tea_reg_no)
		{
			this.tea_reg_no = tea_reg_no;
			InitializeComponent();
			DB.initializedDB();
		}
		
		private void btn_search_Click(object sender, EventArgs e)
		{
			Search_student form_search_stu = new Search_student();
			form_search_stu.Show();
		}

		private void btn_send_Click(object sender, EventArgs e)
		{
			String from = txt_from.Text;
			String to = txt_to.Text;
			String subject = txt_subject.Text;
			String body = txt_body.Text;
			String password = text_pass.Text;
			try
			{
				SmtpClient client = new SmtpClient();
				client.Host = "smtp.gmail.com";
				client.Port = 587;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.UseDefaultCredentials = false;
				client.EnableSsl = true;
				client.Credentials = new NetworkCredential(from, password);
				client.Send(from, to, subject, body);
				MessageBox.Show("Email send Successfully!!!");
			}catch(Exception ex)
			{
				MessageBox.Show("Please check the email address\n Not send");
				MessageBox.Show(ex.ToString());
			}
			clean_txt();
			
		}
		private void clean_txt()
		{
			txt_from.Text = "";
			txt_to.Text = "";
			txt_subject.Text = "";
			txt_body.Text = "";
			text_pass.Text = "";
			
;		}
		private void pictureBox1_Click(object sender, EventArgs e)
		{
			String sql = "SELECT s.name_with_initials, e.email " +
				"FROM student_info s, email e " +
				"WHERE s.reg_no=e.reg_no AND s.reg_no='"+stu_reg_no+"'";
			var record = DB.getData(sql);
			while (record.Read())
			{
				txt_to.Text = record["email"].ToString();
				lbl_stu_name.Text = record["name_with_initials"].ToString();
			}

			sql = "SELECT email " +
					"FROM teacher_info " +
					"WHERE reg_no='"+this.tea_reg_no+"'";

			record = DB.getData(sql);

			while (record.Read())
			{
				txt_from.Text = record["email"].ToString();
			}

		}
	}
}
