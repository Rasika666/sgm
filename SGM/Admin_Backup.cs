﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGM
{
	public partial class Admin_Backup : Form
	{
		public Admin_Backup()
		{
			InitializeComponent();
		}

		Boolean flag = false;
		

		

		private void button1_Click(object sender, EventArgs e)
		{
			Backup();
		}

		private void Backup()
		{
			MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
			builder.Server = txt_server.Text;
			builder.UserID = txt_un.Text;
			builder.Password = txt_pass.Text;
			builder.Database = txt_db.Text;

			String connString = builder.ToString();
			builder = null;

			//progress bra
			this.timer1.Start();
			lbl_status.Text = "Progress ...";
			string file = "H:\\campuz\\3 yr\\2sem\\c#\\project\\sgm\\backup\\"+txt_back_file.Text+".sql";
			//create file
			StreamWriter streamWriter = new StreamWriter(file);
			streamWriter.Close();
			try
			{
				using (MySqlConnection conn = new MySqlConnection(connString))
				{
					using (MySqlCommand cmd = new MySqlCommand())
					{
						using (MySqlBackup mb = new MySqlBackup(cmd))
						{
							cmd.Connection = conn;
							conn.Open();
							mb.ExportToFile(file);
							conn.Close();
						}
					}
				}
				flag = true;
			}
			catch(Exception e)
			{
				
			}
			
			
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			this.progressBar1.Increment(1);
			lbl_progess.Text = progressBar1.Value.ToString()+ " %";
			if (flag)
			{
				lbl_status.Text = "Complete !!!";
			}
			else
			{
				lbl_status.Text = "Failed !!!";
			}
			
		}
	}
}
