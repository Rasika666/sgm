﻿namespace SGM
{
	partial class Stu_dashboard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.label1 = new System.Windows.Forms.Label();
			this.chartMaths = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.lbl_full_name = new System.Windows.Forms.Label();
			this.lbl_class = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.chartMaths)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Dashboard";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// chartMaths
			// 
			this.chartMaths.BackColor = System.Drawing.Color.LightSlateGray;
			chartArea1.Name = "ChartArea1";
			this.chartMaths.ChartAreas.Add(chartArea1);
			this.chartMaths.Dock = System.Windows.Forms.DockStyle.Fill;
			legend1.Name = "Legend1";
			this.chartMaths.Legends.Add(legend1);
			this.chartMaths.Location = new System.Drawing.Point(4, 124);
			this.chartMaths.Margin = new System.Windows.Forms.Padding(4);
			this.chartMaths.Name = "chartMaths";
			series1.ChartArea = "ChartArea1";
			series1.Legend = "Legend1";
			series1.Name = "Series1";
			this.chartMaths.Series.Add(series1);
			this.chartMaths.Size = new System.Drawing.Size(822, 330);
			this.chartMaths.TabIndex = 1;
			this.chartMaths.Text = "chart1";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.chartMaths, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.lbl_full_name, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.lbl_class, 0, 2);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 33);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(830, 478);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// lbl_full_name
			// 
			this.lbl_full_name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_full_name.AutoSize = true;
			this.lbl_full_name.Font = new System.Drawing.Font("Open Sans SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_full_name.ForeColor = System.Drawing.Color.White;
			this.lbl_full_name.Location = new System.Drawing.Point(3, 20);
			this.lbl_full_name.Name = "lbl_full_name";
			this.lbl_full_name.Size = new System.Drawing.Size(824, 50);
			this.lbl_full_name.TabIndex = 2;
			this.lbl_full_name.Text = "label2";
			this.lbl_full_name.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			
			// 
			// lbl_class
			// 
			this.lbl_class.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_class.AutoSize = true;
			this.lbl_class.Font = new System.Drawing.Font("Open Sans SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_class.ForeColor = System.Drawing.Color.White;
			this.lbl_class.Location = new System.Drawing.Point(3, 70);
			this.lbl_class.Name = "lbl_class";
			this.lbl_class.Size = new System.Drawing.Size(824, 50);
			this.lbl_class.TabIndex = 3;
			this.lbl_class.Text = "label2";
			this.lbl_class.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Stu_dashboard
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "Stu_dashboard";
			this.Text = "Stu_dashboard";
			((System.ComponentModel.ISupportInitialize)(this.chartMaths)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMaths;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label lbl_full_name;
		private System.Windows.Forms.Label lbl_class;
	}
}