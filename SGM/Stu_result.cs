﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using System.Windows.Forms.DataVisualization.Charting;


namespace SGM
{
	public partial class Stu_result : Form
	{
        string Reg_no;
        string sql2 = "";
        string grade = "";
        string lineColor = "";
        List<string> subject_list = new List<string>();

        public Stu_result( string reg_no)
        {
            Reg_no = reg_no;
            InitializeComponent();
            load_comboBox();
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //get the current grade of the student
        public string getGrade(string reg_no) {
            string grade_var = "";
            String sql1 = String.Format("SELECT grade " +
                                         "FROM  current_stu_grade " +
                                         "WHERE reg_no = '{0}'", reg_no);
            var recode = DB.getData(sql1);
            while (recode.Read())
            {
                grade_var = recode["grade"].ToString();
            }
            return grade_var;
        }

       // load subjects in to combo box
        public void load_comboBox()
        {
            // get subjects using current grade
            switch (getGrade(Reg_no))
            {
                case "6" :
                     sql2 = String.Format("SELECT grade6 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade6 IS NOT NULL");
                    grade = "grade6";
                    break;
                case "7":
                     sql2 = String.Format("SELECT grade7 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade7 IS NOT NULL");
                    grade = "grade7";
                    break;
                case "8":
                    sql2 = String.Format("SELECT grade8 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade8 IS NOT NULL");
                    grade = "grade8";
                    break;
                case "9":
                     sql2 = String.Format("SELECT grade9 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade9 IS NOT NULL");
                    grade = "grade9";
                    break;
                case "10":
                     sql2 = String.Format("SELECT grade10 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade10 IS NOT NULL");
                    grade = "grade10";
                    break;
                case "11":
                    sql2 = String.Format("SELECT grade11 " +
                                         "FROM  grade_subjects "+
                                         "WHERE grade11 IS NOT NULL");
                    grade = "grade11";
                    break;
                default:
                    break;
            }
            var recode = DB.getData(sql2);
            while (recode.Read())
            {
                cmbsubject.Items.Add(recode[grade]);
            }
        }

        // load the values into chart
        public void load_chart(string subject, string lineColor) {
            var chart = chart1.ChartAreas[0];
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = 45;
            chart.AxisX.IntervalType = DateTimeIntervalType.Number;

            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;

            chart.AxisX.Minimum = 1;
            //chart.AxisX.Maximum = 24;
            chart.AxisY.Minimum = 0;
            //chart.AxisY.Maximum = 100;
            chart.AxisX.Interval = 1;
            chart.AxisY.Interval = 5;

            chart1.Series.Add(subject);
            chart1.Series[subject].ChartType = SeriesChartType.Line;
            // spline will smooth the line
            chart1.Series[subject].Color = Color.FromName(lineColor);
            chart1.Series[0].IsVisibleInLegend = false;

            String sql = String.Format("SELECT {0} as y, CONCAT('grade',grade,' ', 'term',sem) as x " +
                                        "FROM  marks " +
                                        "WHERE reg_no = '{1}'" +
                                        "ORDER 	BY	grade,sem", subject ,Reg_no);
            var record = DB.getData(sql);
            try {
                label5.Text = "";
                while (record.Read())
                {
                    chart1.Series[subject].Points.AddXY(record["x"], record["y"]);
                }
            } catch (Exception) {
                label5.Text = "that subject is not done by you";
            }
           
        }
        //creating chart when select an subject
        private void cmbsubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedSubject = this.cmbsubject.GetItemText(this.cmbsubject.SelectedItem);
            if (lineColor == "")
                label5.Text = "Please select a color";
            else if (!subject_list.Contains(selectedSubject))
            {
                load_chart(selectedSubject, lineColor);
                subject_list.Add(selectedSubject);
                label5.Text = "";
            }
        }

        //select color for the subject in line chart
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lineColor = this.cmbColor.GetItemText(this.cmbColor.SelectedItem);
        }
    }
}
