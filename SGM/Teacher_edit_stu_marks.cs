﻿using SGM.database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGM
{
	public partial class Teacher_edit_stu_marks : Form
	{
		public static String regno;
		public Teacher_edit_stu_marks()
		{
			InitializeComponent();
			DB.initializedDB();
			disable_txt();
		}


		
		

		private void btn_edit_Click(object sender, EventArgs e)
		{
			txt_math.ReadOnly = false;
			txt_science.ReadOnly = false;
			txt_sinhala.ReadOnly = false;
			txt_religion.ReadOnly = false;
			txt_music.ReadOnly = false;
			txt_dance.ReadOnly = false;
			txt_commerce.ReadOnly = false;
			txt_it.ReadOnly = false;
			txt_elit.ReadOnly = false;
			txt_slit.ReadOnly = false;
			txt_media.ReadOnly = false;
			txt_history.ReadOnly = false;
			txt_health.ReadOnly = false;
			txt_geo.ReadOnly = false;
			txt_art.ReadOnly = false;
			txt_tamil.ReadOnly = false;
		}
		private void disable_txt()
		{
			txt_math.ReadOnly = true;
			txt_science.ReadOnly = true;
			txt_sinhala.ReadOnly = true;
			txt_religion.ReadOnly = true;
			txt_music.ReadOnly = true;
			txt_dance.ReadOnly = true;
			txt_commerce.ReadOnly = true;
			txt_it.ReadOnly = true;
			txt_elit.ReadOnly = true;
			txt_slit.ReadOnly = true;
			txt_media.ReadOnly = true;
			txt_history.ReadOnly = true;
			txt_health.ReadOnly = true;
			txt_geo.ReadOnly = true;
			txt_art.ReadOnly = true;
			txt_tamil.ReadOnly = true;
		}
		

		private void btn_update_Click_1(object sender, EventArgs e)
		{
			String sql = "UPDATE marks SET maths = '" + txt_math.Text + "'," +
							"science = '" + txt_science.Text + "'," +
							"religion = '" + txt_religion.Text + "'," +
							"english = '" + txt_english.Text + "'," +
							"music = '" + txt_music.Text + "'," +
							"dancing = '" + txt_dance.Text + "'," +
							"commerce = '" + txt_commerce.Text + "'," +
							"it = '" + txt_it.Text + "'," +
							//"E.Lit = '" + txt_elit.Text + "'," +
							//"s.lit = '" + txt_slit.Text + "'," +
							"media = '" + txt_media.Text + "'," +
							"history = '" + txt_history.Text + "'," +
							"Health_science = '" + txt_health.Text + "'," +
							"geography = '" + txt_geo.Text + "'," +
							"art = '" + txt_art.Text + "'," +
							"dramma = '" + txt_dramma.Text + "'," +
							"tamil = '" + txt_tamil.Text + "' " +
							"WHERE reg_no = '"+regno+"'";
			if(DB.putData(sql) > 0)
			{
				MessageBox.Show("Successfully Updated");
			}
			else
			{
				MessageBox.Show("Somthing went Wrong!!!");
			}
		}
		

		private void btn_search_Click(object sender, EventArgs e)
		{
			teacher_mark form_search_stu = new teacher_mark();
			form_search_stu.Show();
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			
			String sql = "SELECT * FROM marks WHERE reg_no = '"+regno+"'";
			var rec = DB.getData(sql);
			while (rec.Read())
			{
				txt_math.Text = rec["maths"].ToString();
				txt_science.Text = rec["science"].ToString();
				txt_sinhala.Text = rec["sinhala"].ToString();
				txt_religion.Text = rec["religion"].ToString();
				txt_music.Text = rec["music"].ToString();
				txt_dance.Text = rec["dancing"].ToString();
				txt_commerce.Text = rec["commerce"].ToString();
				txt_it.Text = rec["it"].ToString();
				txt_elit.Text = rec["e.lit"].ToString();
				txt_slit.Text = rec["s.lit"].ToString();
				txt_media.Text = rec["media"].ToString();
				txt_history.Text = rec["history"].ToString();
				txt_health.Text = rec["health_science"].ToString();
				txt_geo.Text = rec["geography"].ToString();
				txt_art.Text = rec["art"].ToString();
				txt_tamil.Text = rec["tamil"].ToString();
			}
		}
	}
}
