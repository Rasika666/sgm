﻿namespace SGM
{
	partial class teacher_mark
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(teacher_mark));
			this.pnl_barTop = new System.Windows.Forms.Panel();
			this.btn_minimized = new System.Windows.Forms.PictureBox();
			this.btn_maximize = new System.Windows.Forms.PictureBox();
			this.btn_cancel_panel = new System.Windows.Forms.PictureBox();
			this.btn_resize = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.combo_grade = new System.Windows.Forms.ComboBox();
			this.combo_class = new System.Windows.Forms.ComboBox();
			this.combo_name = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button2 = new System.Windows.Forms.Button();
			this.pnl_barTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_barTop
			// 
			this.pnl_barTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.pnl_barTop.Controls.Add(this.btn_minimized);
			this.pnl_barTop.Controls.Add(this.btn_maximize);
			this.pnl_barTop.Controls.Add(this.btn_cancel_panel);
			this.pnl_barTop.Controls.Add(this.btn_resize);
			this.pnl_barTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnl_barTop.Location = new System.Drawing.Point(0, 0);
			this.pnl_barTop.Name = "pnl_barTop";
			this.pnl_barTop.Size = new System.Drawing.Size(1014, 40);
			this.pnl_barTop.TabIndex = 0;
			this.pnl_barTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_barTop_MouseDown);
			// 
			// btn_minimized
			// 
			this.btn_minimized.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_minimized.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_minimized.Image = global::SGM.Properties.Resources.minus;
			this.btn_minimized.Location = new System.Drawing.Point(892, 9);
			this.btn_minimized.Name = "btn_minimized";
			this.btn_minimized.Size = new System.Drawing.Size(25, 25);
			this.btn_minimized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_minimized.TabIndex = 0;
			this.btn_minimized.TabStop = false;
			this.btn_minimized.Click += new System.EventHandler(this.btn_minimized_Click);
			// 
			// btn_maximize
			// 
			this.btn_maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_maximize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_maximize.Image = global::SGM.Properties.Resources.maximize;
			this.btn_maximize.Location = new System.Drawing.Point(934, 9);
			this.btn_maximize.Name = "btn_maximize";
			this.btn_maximize.Size = new System.Drawing.Size(25, 25);
			this.btn_maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_maximize.TabIndex = 0;
			this.btn_maximize.TabStop = false;
			this.btn_maximize.Click += new System.EventHandler(this.btn_maximize_Click);
			// 
			// btn_cancel_panel
			// 
			this.btn_cancel_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_cancel_panel.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_cancel_panel.Image = global::SGM.Properties.Resources.cancel;
			this.btn_cancel_panel.Location = new System.Drawing.Point(977, 9);
			this.btn_cancel_panel.Name = "btn_cancel_panel";
			this.btn_cancel_panel.Size = new System.Drawing.Size(25, 25);
			this.btn_cancel_panel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_cancel_panel.TabIndex = 0;
			this.btn_cancel_panel.TabStop = false;
			this.btn_cancel_panel.Click += new System.EventHandler(this.btn_cancel_Click);
			// 
			// btn_resize
			// 
			this.btn_resize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_resize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_resize.Image = global::SGM.Properties.Resources.scale;
			this.btn_resize.Location = new System.Drawing.Point(934, 9);
			this.btn_resize.Name = "btn_resize";
			this.btn_resize.Size = new System.Drawing.Size(25, 25);
			this.btn_resize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_resize.TabIndex = 0;
			this.btn_resize.TabStop = false;
			this.btn_resize.Click += new System.EventHandler(this.btn_resize_Click);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 6;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
			this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label2, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label3, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.combo_grade, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.combo_class, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.combo_name, 4, 1);
			this.tableLayoutPanel1.Controls.Add(this.button1, 4, 4);
			this.tableLayoutPanel1.Controls.Add(this.listView1, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.textBox1, 4, 2);
			this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 5, 2);
			this.tableLayoutPanel1.Controls.Add(this.button2, 4, 6);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 40);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 7;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1014, 628);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(53, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Grade";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(53, 70);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 27);
			this.label2.TabIndex = 0;
			this.label2.Text = "Term";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(503, 20);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 27);
			this.label3.TabIndex = 0;
			this.label3.Text = "Name";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(503, 70);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(81, 27);
			this.label4.TabIndex = 0;
			this.label4.Text = "Reg No";
			// 
			// combo_grade
			// 
			this.combo_grade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.combo_grade.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.combo_grade.FormattingEnabled = true;
			this.combo_grade.Location = new System.Drawing.Point(188, 23);
			this.combo_grade.Name = "combo_grade";
			this.combo_grade.Size = new System.Drawing.Size(309, 28);
			this.combo_grade.TabIndex = 1;
			// 
			// combo_class
			// 
			this.combo_class.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.combo_class.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.combo_class.FormattingEnabled = true;
			this.combo_class.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
			this.combo_class.Location = new System.Drawing.Point(188, 73);
			this.combo_class.Name = "combo_class";
			this.combo_class.Size = new System.Drawing.Size(309, 28);
			this.combo_class.TabIndex = 1;
			// 
			// combo_name
			// 
			this.combo_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.combo_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.combo_name.FormattingEnabled = true;
			this.combo_name.Location = new System.Drawing.Point(638, 23);
			this.combo_name.Name = "combo_name";
			this.combo_name.Size = new System.Drawing.Size(309, 28);
			this.combo_name.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(746, 143);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(201, 34);
			this.button1.TabIndex = 2;
			this.button1.Text = "Search";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// listView1
			// 
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
			this.tableLayoutPanel1.SetColumnSpan(this.listView1, 4);
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.FullRowSelect = true;
			this.listView1.HoverSelection = true;
			this.listView1.LabelEdit = true;
			this.listView1.Location = new System.Drawing.Point(53, 193);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(894, 382);
			this.listView1.TabIndex = 3;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Reg no";
			this.columnHeader1.Width = 200;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 250;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Grade";
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Term";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Email";
			this.columnHeader5.Width = 300;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(638, 73);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(309, 28);
			this.textBox1.TabIndex = 4;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(953, 73);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 5;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.button2.Location = new System.Drawing.Point(803, 581);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(144, 35);
			this.button2.TabIndex = 6;
			this.button2.Text = "OK";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// teacher_mark
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1014, 668);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.pnl_barTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "teacher_mark";
			this.Text = "Form1";
			this.pnl_barTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_barTop;
		private System.Windows.Forms.PictureBox btn_cancel_panel;
		private System.Windows.Forms.PictureBox btn_minimized;
		private System.Windows.Forms.PictureBox btn_maximize;
		private System.Windows.Forms.PictureBox btn_resize;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox combo_grade;
		private System.Windows.Forms.ComboBox combo_class;
		private System.Windows.Forms.ComboBox combo_name;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button2;
	}
}

