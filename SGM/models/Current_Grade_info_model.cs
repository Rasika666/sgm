﻿using SGM.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGM.models
{
	class Current_Grade_info_model : Stu_info_model
	{


		public int grade;
		public  String clss;
		public int sem;
		private  Boolean flag = false;
		

	public Current_Grade_info_model(string reg_no, string name_with_initials, string full_name, string age, string sex, string address, string contact_no, byte[] image, DateTime enter_date, DateTime leave_date, string email,int grade, String cls, int sem) : 
			base(reg_no,  name_with_initials,  full_name,  age,  sex,  address,  contact_no,  image,  enter_date,  leave_date, email)
		{
			this.grade = grade;
			this.clss = cls;
			this.sem = sem;
			flag = true;
		}

	public Current_Grade_info_model(string reg_no, string name_with_initials, string full_name, string age, string sex, string address, string contact_no, byte[] image, DateTime enter_date, DateTime leave_date, string email, int grade, int sem) :
			base(reg_no, name_with_initials, full_name, age, sex, address, contact_no, image, enter_date, leave_date, email)
	{
		this.grade = grade;
		this.sem = sem;
			
	}

		public static List<Current_Grade_info_model> GetStu(String sql,int i=0)
		{
			List<Current_Grade_info_model> stu_list = new List<Current_Grade_info_model>();
			

			var record = DB.getData(sql);

			while (record.Read())
			{
				String reg_no = record["reg_no"].ToString();
				String init = record["name_with_initials"].ToString();
				String fname = record["full_name"].ToString();
				String age = record["age"].ToString();
				String sex = record["sex"].ToString();
				String address = record["address"].ToString();
				String contact = record["contact_no"].ToString();
				byte[] img = (byte[])record["image"];
				String edate = record["enter_date"].ToString();
				DateTime enterdate = DateTime.Parse(edate);
				String ldate = record["leave_date"].ToString();
				DateTime leavedate;
				if (string.IsNullOrEmpty(ldate) || ldate == DateTime.MinValue.ToString())
				{
					leavedate = DateTime.MinValue;
				}
				else
				{
					leavedate = DateTime.Parse(ldate);
				}

				String mail = record["email"].ToString();

				int grade = (int)record["grade"];
				String cls = "";
				if (i==1)
				{
					cls = record["class"].ToString();
				}
				
				int sem = (int)record["sem"];
				Current_Grade_info_model stu;
				if (i==1)
				{
					stu = new Current_Grade_info_model(reg_no, init, fname, age, sex, address, contact, img, enterdate, leavedate, mail, grade, cls, sem);
				}
				else
				{
					stu = new Current_Grade_info_model(reg_no, init, fname, age, sex, address, contact, img, enterdate, leavedate, mail, grade, sem);
				}
				

				stu_list.Add(stu);
			}


			return stu_list;
		}



	}
}
