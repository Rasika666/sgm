﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGM
{
    class student
    {
        public string reg_no { get; private set; }
        public string stu_name { get; private set; }
        public int age { get; private set; }
        public string sex { get; private set; }
        public string address { get; private set; }
        public int contact_no { get; private set; }
        public string enter_date { get; private set; }
        public string email { get; private set; }

        public student(string reg_no, string stu_name, int age, string sex, string address, int contact_no, string enter_date, string email)
        {
            this.reg_no = reg_no;
            this.stu_name = stu_name;
            this.age = age;
            this.sex = sex;
            this.address = address;
            this.contact_no = contact_no;
            this.enter_date = enter_date;
            this.email = email;

        }


    }
}
