﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;

namespace SGM.models
{
	class Stu_info_model
	{
		public String reg_no { get; private set; }
		public String name_with_initials { get; private set; }
		public String full_name { get; private set; }
		public String age { get; private set; }
		public String sex { get; private set; }
		public String Address { get; private set; }
		public String contact_no { get; private set; }
		public Byte[] image { get; private set; }
		public DateTime enter_date { get; private set; }
		public DateTime Leave_date { get; private set; }
		public String email { get; private set; }

		public Stu_info_model(string reg_no, string name_with_initials, string full_name, string age, string sex, string address, string contact_no, byte[] image, DateTime enter_date, DateTime leave_date, string email)
		{
			this.reg_no = reg_no;
			this.name_with_initials = name_with_initials;
			this.full_name = full_name;
			this.age = age;
			this.sex = sex;
			Address = address;
			this.contact_no = contact_no;
			this.image = image;
			this.enter_date = enter_date;
			Leave_date = leave_date;
			this.email = email;

			DB.initializedDB();
		}



		public static List<Stu_info_model> getStu(String rn)
		{
			List<Stu_info_model> stu_list = new List<Stu_info_model>();

			String sql = "SELECT s.*, e.email " +
							"FROM student_info s JOIN email e ON s.reg_no = e.reg_no " +
							"where s.reg_no = '" + rn + "'";

			var record = DB.getData(sql);

			while (record.Read())
			{
				String reg_no = record["reg_no"].ToString();
				String init = record["name_with_initials"].ToString();
				String fname = record["full_name"].ToString();
				String age = record["age"].ToString();
				String sex = record["sex"].ToString();
				String address = record["address"].ToString();
				String contact = record["contact_no"].ToString();
				byte[] img = (byte[])record["image"];
				String edate = record["enter_date"].ToString();
				DateTime enterdate = DateTime.Parse(edate);
				String ldate = record["leave_date"].ToString();
				DateTime leavedate;
				if (string.IsNullOrEmpty(ldate) || ldate == DateTime.MinValue.ToString())
				{
					leavedate = DateTime.MinValue;
				}
				else
				{
					leavedate = DateTime.Parse(ldate);
				}

				String mail = record["email"].ToString();


				Stu_info_model stu = new Stu_info_model(reg_no, init, fname, age, sex, address, contact, img, enterdate, leavedate, mail);

				stu_list.Add(stu);
			}


			return stu_list;
		}

		public void updateStu()
		{
			String sql = "UPDATE student_info s, email e SET s.name_with_initials = '" + this.name_with_initials + "', s.full_name = '" + this.full_name + "', age = '" + this.age + "', sex = '" + this.sex + "', address = '" + this.Address + "', contact_no = '" + this.contact_no + "', enter_date = '" + this.enter_date + "', leave_date = '" + this.Leave_date + "', image = @image ,e.email = '" + this.email + "' WHERE s.reg_no = '" + this.reg_no + "' AND s.reg_no=e.reg_no";

			if (DB.putDataImg(sql, this.image) > 0)
			{
				MessageBox.Show("updated successfully");
			}
			else
			{
				MessageBox.Show("not updateed");
			}
		}
	}
}

