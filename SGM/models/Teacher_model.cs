﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;

namespace SGM.models
{
	class Teacher_model
	{

		public String reg_no { get; private set; }
		public String name { get; private set; }
		public String age { get; private set; }
		public String sex { get; private set; }
		public String Address { get; private set; }
		public String contact_no { get; private set; }
		public Byte[] image { get; private set; }
		public DateTime enter_date { get; private set; }
		public DateTime Leave_date { get; private set; }
		public String email { get; private set; }

		public Teacher_model(string reg_no, string name, string age, string sex, string address, string contact_no, byte[] image, DateTime enter_date, DateTime leave_date, string email)
		{
			this.reg_no = reg_no;
			this.name = name;
			this.age = age;
			this.sex = sex;
			Address = address;
			this.contact_no = contact_no;
			this.image = image;
			this.enter_date = enter_date;
			Leave_date = leave_date;
			this.email = email;
		}


		public static List<Teacher_model> getTeacher(String tn)
		{
			List<Teacher_model> tea_list = new List<Teacher_model>();

			String sql = "SELECT * FROM teacher_info WHERE reg_no='" + tn + "'";

			var record = DB.getData(sql);

			while (record.Read())
			{
				String reg_no = record["reg_no"].ToString();
				String name = record["tea_name"].ToString();
				String age = record["age"].ToString();
				String sex = record["sex"].ToString();
				String address = record["address"].ToString();
				String contact = record["contact_no"].ToString();
				byte[] img = (byte[])record["image"];
				String edate = record["enter_date"].ToString();
				DateTime enterdate = DateTime.Parse(edate);
				String ldate = record["leave_date"].ToString();
				DateTime leavedate;
				if (string.IsNullOrEmpty(ldate) || ldate == DateTime.MinValue.ToString())
				{
					leavedate = DateTime.MinValue;
				}
				else
				{
					leavedate = DateTime.Parse(ldate);
				}

				String mail = record["email"].ToString();


				Teacher_model teacher = new Teacher_model(reg_no, name, age, sex, address, contact, img, enterdate, leavedate, mail);

				tea_list.Add(teacher);
			}


			return tea_list;
		}

		public void updateTeacher()
		{
			String sql = "UPDATE teacher_info SET tea_name='"+this.name+"',age='"+this.age+"',sex='"+this.age+"', address='"+this.Address+"',contact_no='"+this.contact_no+"',image=@image,enter_date='"+this.enter_date+"',leave_date='"+this.Leave_date+"',email='"+this.email+"' WHERE reg_no='"+this.reg_no+"'";

			if (DB.putDataImg(sql, this.image) > 0)
			{
				MessageBox.Show("updated successfully");
			}
			else
			{
				MessageBox.Show("not updateed");
			}
		}

	}
}
