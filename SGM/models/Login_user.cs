﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGM.database;

namespace SGM.models
{
	class Login_user
	{
		

		public int id { get; private set; }
		public String reg_no { get; private set; }
		public String username { get; private set; }
		public String password { get; private set; }
		public String member_state { get; private set; }

		public Login_user(int id, string reg_no, string username, string password, string member_state)
		{
			this.id = id;
			this.reg_no = reg_no;
			this.username = username;
			this.password = password;
			this.member_state = member_state;
		}

		public static List<Login_user> getUsers(String un)
		{

			List<Login_user> cu = new List<Login_user>();


			String sql = String.Format("SELECT * FROM login " +
										"WHERE user_name='{0}'",un);

			var rec = DB.getData(sql);
			while (rec.Read())
			{
				int id = (int)rec["id"];
				String reg_no = rec["reg_no"].ToString();
				String username = rec["user_name"].ToString();
				String pass = rec["password"].ToString();
				String state = rec["member_state"].ToString();

				Login_user user = new Login_user(id, reg_no, username, pass, state);
				cu.Add(user);
			}

			/****************************************************************
			 * if user is exist , we have only one user
			 * when we reterive the cu list
			 * we use 0 index
			 *
			 * **************************************************************/
			return cu;
			
		}
	}
}
