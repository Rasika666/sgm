﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SGM.database;
using System.Windows.Forms;
namespace SGM.models
{
    class teacher
    {


        //attributes
        public int id { get; private set; }
        public String reg_no { get; private set; }
        public String tea_name { get; private set; }
        public int age { get; private set; }
        public String sex { get; private set; }
        public String address { get; private set; }
        public String contact_no { get; private set; }
        public String enter_date { get; private set; }
        public String leave_date { get; private set; }
        public String email { get; private set; }

        public teacher(int id, String reg_no, String tea_name, int age, String sex, String address, String contact_no, String enter_date, String leave_date, String email)
        {
            this.id = id;
            this.reg_no = reg_no;
            this.tea_name = tea_name;
            this.age = age;
            this.sex = sex;
            this.address = address;
            this.contact_no = contact_no;
            this.enter_date = enter_date;
            this.leave_date = leave_date;
            this.email = email;
        }

        public teacher()
        {
        }

        public static List<teacher> getUsers()
        {
            List<teacher> techer1 = new List<teacher>();

            //fetch data
            String query = "SELECT * FROM teacher_info";
            var record = DB.getData(query);


            while (record.Read())
            {
                int id = (int)record["id"];
                String reg_no = record["reg_no"].ToString();
                String tea_name = record["tea_name"].ToString();
                int age = (int)record["age"];
                String sex = record["sex"].ToString();
                String address = record["address"].ToString();
                String contact_no = record["contact_no"].ToString();
                String enter_date = record["enter_date"].ToString();
                String leave_date = record["leave_date"].ToString();
                String email = record["email"].ToString();


                teacher u = new teacher(id, reg_no, tea_name, age, sex, address, contact_no, enter_date, leave_date, email);

                techer1.Add(u);
            }




            return techer1;

        }

        public static void insert(String a, String b, int c, String d, String e, String f, String g, String h, String i)
        {
            String query = String.Format("INSERT INTO teacher_info (reg_no,tea_name,age,sex,address,contact_no,enter_date,leave_date,email)" +
                                        "VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", a, b, c, d, e, f, g, h, i);
            if (DB.putData(query) > 0)
            {
                MessageBox.Show("insert succsecefull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
            }
        }

        public void update(String b, int c, String d, String e, String f, String g, String h, String i)
        {
           
            String query = String.Format("UPDATE teacher_info SET  tea_name='{0}', age='{1}', sex='{2}', address='{3}', contact_no='{4}', enter_date='{5}', leave_date='{6}', email='{7}' WHERE id = '{8}'", b, c, d, e, f, g, h, i, id);
            if (DB.putData(query) > 0) 
            {
                MessageBox.Show("Update Successfull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
            }
        }



        public void delete(int id)
        {
            String query = String.Format("DELETE FROM teacher_info WHERE id = '{0}'",id);
            if (DB.putData(query) > 0)
            {
                MessageBox.Show("delete Successfull");
            }
            else
            {
                MessageBox.Show("Somthing went Wrong \n try again");
            }
        }
    }



}

