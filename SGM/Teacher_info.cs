﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using SGM.models;

namespace SGM
{
	public partial class Teacher_info : Form
	{
		private string regno;
		public Teacher_info(String regno)
		{
			InitializeComponent();
			this.regno = regno;
			init_disable_txt();
			load_txt();
			DB.initializedDB();
		}

		private void init_disable_txt()
		{
			txt_address.ReadOnly = true;
			txt_age.ReadOnly = true;
			txt_contact.ReadOnly = true;
			txt_name.ReadOnly = true;
			
		}

		String imageLoc = "";

		private void btn_browse_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				imageLoc = dialog.FileName.ToString();
				pb_avatar.ImageLocation = imageLoc;
			}
		}

		private void load_txt()
		{
			List<Teacher_model> teacher = Teacher_model.getTeacher(this.regno);



			txt_regno.Text = this.regno;
			txt_name.Text = teacher[0].name;
			
			txt_age.Text = teacher[0].age;
			//sex here
			if (teacher[0].sex == "male")
			{
				radioButton1.Checked = true;
			}
			else
			{
				radioButton2.Checked = true;
			}
			txt_address.Text = teacher[0].Address;
			txt_contact.Text = teacher[0].contact_no;
			//load img
			byte[] img = teacher[0].image;
			//convert binary to image
			MemoryStream ms = new MemoryStream(img);
			pb_avatar.Image = System.Drawing.Image.FromStream(ms);
			txt_edate.Text = teacher[0].enter_date.ToString();
			if (teacher[0].Leave_date.ToString() == DateTime.MinValue.ToString())
			{
				txt_ldate.Text = "";
			}
			else
			{
				txt_ldate.Text = teacher[0].Leave_date.ToString();
			}
			txt_email.Text = teacher[0].email;

		}

		private void btn_edit_Click(object sender, EventArgs e)
		{
			txt_address.ReadOnly = false;
			txt_age.ReadOnly = false;
			txt_contact.ReadOnly = false;
			txt_name.ReadOnly = false;
			txt_email.ReadOnly = false;
			
		}

		private void btn_update_Click(object sender, EventArgs e)
		{
			String reg_no = txt_regno.Text;
			String name = txt_name.Text;
			String age = txt_age.Text;
			//sex
			String sex = "";
			if (radioButton1.Checked == true)
			{
				sex = "male";
			}
			if (radioButton2.Checked == true)
			{
				sex = "female";
			}
			String add = txt_address.Text;
			String contact = txt_contact.Text;
			//image
			byte[] img = imgToByteArray(pb_avatar.Image);
			//dates theDate.ToString("yyyy-MM-dd H:mm:ss");
			DateTime edate = DateTime.Parse(txt_edate.Text);
			DateTime ldate;
			if (string.IsNullOrEmpty(txt_ldate.Text))
			{
				ldate = DateTime.MinValue;
			}
			else
			{
				ldate = DateTime.Parse(txt_ldate.Text);
			}
			String email = txt_email.Text;

			Teacher_model updateTea = new Teacher_model(reg_no, name, age, sex, add, contact, img, edate, ldate, email);

			updateTea.updateTeacher();
		}

		public byte[] imgToByteArray(Image img)
		{
			using (MemoryStream mStream = new MemoryStream())
			{
				img.Save(mStream, img.RawFormat);
				return mStream.ToArray();
			}
		}
	}
}
