﻿namespace SGM
{
	partial class Teacher_email
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Teacher_email));
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.txt_from = new System.Windows.Forms.TextBox();
			this.text_pass = new System.Windows.Forms.TextBox();
			this.txt_to = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txt_subject = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txt_body = new System.Windows.Forms.RichTextBox();
			this.btn_search = new System.Windows.Forms.Button();
			this.btn_send = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.lbl_stu_name = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Email";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 6;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
			this.tableLayoutPanel1.Controls.Add(this.label2, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label3, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label4, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_from, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.text_pass, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.txt_to, 4, 1);
			this.tableLayoutPanel1.Controls.Add(this.label5, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.txt_subject, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.label6, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_body, 2, 4);
			this.tableLayoutPanel1.Controls.Add(this.btn_search, 2, 6);
			this.tableLayoutPanel1.Controls.Add(this.btn_send, 4, 6);
			this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 5, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 7;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(835, 487);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label2.Location = new System.Drawing.Point(53, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 27);
			this.label2.TabIndex = 0;
			this.label2.Text = "From";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label3.Location = new System.Drawing.Point(53, 70);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(103, 27);
			this.label3.TabIndex = 0;
			this.label3.Text = "Passowrd";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label4.Location = new System.Drawing.Point(419, 20);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(35, 27);
			this.label4.TabIndex = 0;
			this.label4.Text = "To";
			// 
			// txt_from
			// 
			this.txt_from.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_from.Location = new System.Drawing.Point(163, 23);
			this.txt_from.Name = "txt_from";
			this.txt_from.Size = new System.Drawing.Size(250, 30);
			this.txt_from.TabIndex = 1;
			// 
			// text_pass
			// 
			this.text_pass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.text_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.text_pass.Location = new System.Drawing.Point(163, 73);
			this.text_pass.Name = "text_pass";
			this.text_pass.Size = new System.Drawing.Size(250, 30);
			this.text_pass.TabIndex = 1;
			// 
			// txt_to
			// 
			this.txt_to.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_to.Location = new System.Drawing.Point(529, 23);
			this.txt_to.Name = "txt_to";
			this.txt_to.Size = new System.Drawing.Size(250, 30);
			this.txt_to.TabIndex = 1;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label5.Location = new System.Drawing.Point(53, 120);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 27);
			this.label5.TabIndex = 0;
			this.label5.Text = "Subject";
			// 
			// txt_subject
			// 
			this.txt_subject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_subject, 3);
			this.txt_subject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_subject.Location = new System.Drawing.Point(163, 123);
			this.txt_subject.Name = "txt_subject";
			this.txt_subject.Size = new System.Drawing.Size(616, 30);
			this.txt_subject.TabIndex = 1;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label6.Location = new System.Drawing.Point(53, 199);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(59, 27);
			this.label6.TabIndex = 0;
			this.label6.Text = "Body";
			// 
			// txt_body
			// 
			this.txt_body.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.txt_body, 3);
			this.txt_body.Location = new System.Drawing.Point(163, 202);
			this.txt_body.Name = "txt_body";
			this.txt_body.Size = new System.Drawing.Size(616, 152);
			this.txt_body.TabIndex = 2;
			this.txt_body.Text = "";
			// 
			// btn_search
			// 
			this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_search.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_search.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_search.Location = new System.Drawing.Point(163, 410);
			this.btn_search.Name = "btn_search";
			this.btn_search.Size = new System.Drawing.Size(183, 47);
			this.btn_search.TabIndex = 3;
			this.btn_search.Text = "Search";
			this.btn_search.UseVisualStyleBackColor = false;
			this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
			// 
			// btn_send
			// 
			this.btn_send.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_send.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_send.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_send.ForeColor = System.Drawing.Color.Red;
			this.btn_send.Location = new System.Drawing.Point(529, 410);
			this.btn_send.Name = "btn_send";
			this.btn_send.Size = new System.Drawing.Size(183, 47);
			this.btn_send.TabIndex = 3;
			this.btn_send.Text = "Send";
			this.btn_send.UseVisualStyleBackColor = false;
			this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(785, 73);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// lbl_stu_name
			// 
			this.lbl_stu_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lbl_stu_name.AutoSize = true;
			this.lbl_stu_name.Font = new System.Drawing.Font("Open Sans SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_stu_name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.lbl_stu_name.Location = new System.Drawing.Point(563, 2);
			this.lbl_stu_name.Name = "lbl_stu_name";
			this.lbl_stu_name.Size = new System.Drawing.Size(89, 27);
			this.lbl_stu_name.TabIndex = 2;
			this.lbl_stu_name.Text = "Student";
			// 
			// Teacher_email
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.lbl_stu_name);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Teacher_email";
			this.Text = "Teacher_email";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txt_from;
		private System.Windows.Forms.TextBox text_pass;
		private System.Windows.Forms.TextBox txt_to;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txt_subject;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.RichTextBox txt_body;
		private System.Windows.Forms.Button btn_search;
		private System.Windows.Forms.Button btn_send;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label lbl_stu_name;
	}
}