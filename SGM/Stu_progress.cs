﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGM.database;
using System.Windows.Forms.DataVisualization.Charting;

namespace SGM
{
	public partial class Stu_progress : Form
	{
        string Reg_no;
        string sql2;
        string sql3;
        string grade;
        string subject = "";
        string student = "";
		public Stu_progress(string reg_no)
		{
            Reg_no = reg_no;
            InitializeComponent();
            load_comboBox();
		}

        //get the current grade of the student
        public string getGrade(string reg_no)
        {
            string grade_var = "";
            String sql1 = String.Format("SELECT grade " +
                                         "FROM  current_stu_grade " +
                                         "WHERE reg_no = '{0}'", reg_no);
            var recode = DB.getData(sql1);
            while (recode.Read())
            {
                grade_var = recode["grade"].ToString();
            }
            return grade_var;
        }

        // load subjects in to combo box
        public void load_comboBox()
        {
            // get subjects using current grade
            switch (getGrade(Reg_no))
            {
                case "6":
                    sql2 = String.Format("SELECT grade6 " +
                                        "FROM  grade_subjects " +
                                        "WHERE grade6 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 6");
                    grade = "grade6";
                    break;
                case "7":
                    sql2 = String.Format("SELECT grade7 " +
                                        "FROM  grade_subjects " +
                                        "WHERE grade7 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 7");
                    grade = "grade7";
                    break;
                case "8":
                    sql2 = String.Format("SELECT grade8 " +
                                         "FROM  grade_subjects " +
                                         "WHERE grade8 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 8");
                    grade = "grade8";
                    break;
                case "9":
                    sql2 = String.Format("SELECT grade9 " +
                                        "FROM  grade_subjects " +
                                        "WHERE grade9 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 9");
                    grade = "grade9";
                    break;
                case "10":
                    sql2 = String.Format("SELECT grade10 " +
                                        "FROM  grade_subjects " +
                                        "WHERE grade10 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 10");
                    grade = "grade10";
                    break;
                case "11":
                    sql2 = String.Format("SELECT grade11 " +
                                         "FROM  grade_subjects " +
                                         "WHERE grade11 IS NOT NULL");

                    sql3 = String.Format("SELECT reg_no " +
                                        "FROM  current_stu_grade " +
                                        "WHERE grade = 11");
                    grade = "grade11";
                    break;
                default:
                    break;
            }
            //filling subject
            var recode1 = DB.getData(sql2);
            while (recode1.Read())
            {
                cmbSubject.Items.Add(recode1[grade]);
            }

            //filing student
            var recode2 = DB.getData(sql3);
            while (recode2.Read())
            {
                cmbStudent.Items.Add(recode2["reg_no"]);
            }
        }

        public void createGraph(string other_reg_no, string subject) {
            //clear chart
            foreach (var series in chartProgress.Series)
            {
                series.Points.Clear();
            }
            chartProgress.Series.Clear();

            //create chart
            var chart = chartProgress.ChartAreas[0];
            chartProgress.ChartAreas[0].AxisX.LabelStyle.Angle = 45;
            chart.AxisX.IntervalType = DateTimeIntervalType.Number;

            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;

            chart.AxisX.Minimum = 1;
            //chart.AxisX.Maximum = 24;
            chart.AxisY.Minimum = 0;
            //chart.AxisY.Maximum = 100;
            chart.AxisX.Interval = 1;
            chart.AxisY.Interval = 5;
            //line for current student
            chartProgress.Series.Add(Reg_no);
            chartProgress.Series[Reg_no].ChartType = SeriesChartType.Spline;
            chartProgress.Series[Reg_no].Color = Color.Green;
            chartProgress.Series[0].IsVisibleInLegend = false;

            String sql4 = String.Format("SELECT {0} as y, CONCAT('grade',grade,' ', 'term',sem) as x " +
                                        "FROM  marks " +
                                        "WHERE reg_no = '{1}'" +
                                        "ORDER 	BY	grade,sem", subject, Reg_no);
            var record4 = DB.getData(sql4);
            try
            {
                while (record4.Read())
                {
                    chartProgress.Series[Reg_no].Points.AddXY(record4["x"], record4["y"]);
                }
            }
            catch (Exception) {
                lblerr.Text = "That subject is not done by "+ Reg_no;
            }
            

            if(student != Reg_no){
                //line for other student
                chartProgress.Series.Add(other_reg_no);
                chartProgress.Series[other_reg_no].ChartType = SeriesChartType.Spline;
                chartProgress.Series[other_reg_no].Color = Color.Red;
                chartProgress.Series[0].IsVisibleInLegend = false;

                String sql5 = String.Format("SELECT {0} as y, CONCAT('grade',grade,' ', 'term',sem) as x " +
                                            "FROM  marks " +
                                            "WHERE reg_no = '{1}'" +
                                            "ORDER 	BY	grade,sem", subject, other_reg_no);
                var record5 = DB.getData(sql5);
                try {
                    while (record5.Read())
                    {
                        chartProgress.Series[other_reg_no].Points.AddXY(record5["x"], record5["y"]);
                    }
                }
                catch (Exception){
                    lblerr.Text = subject +" is not done by "+ other_reg_no;
                }
                
            }
            

        }

        private void cmbStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            student = this.cmbStudent.GetItemText(this.cmbStudent.SelectedItem);
        }

        private void cmbSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (student == "")
            {
                lblerr.Text = "please select a student";
            }
            else {
                lblerr.Text = "";
                subject = this.cmbSubject.SelectedItem.ToString();
                createGraph(student, subject);
            }
        }

        

    }
    
   
}
