﻿namespace SGM
{
	partial class layout_teacher
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(layout_teacher));
			this.pnl_barTop = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.ovalPictureBox1 = new OvalPictureBox();
			this.btn_minimized = new System.Windows.Forms.PictureBox();
			this.btn_maximize = new System.Windows.Forms.PictureBox();
			this.btn_cancel_panel = new System.Windows.Forms.PictureBox();
			this.btn_resize = new System.Windows.Forms.PictureBox();
			this.pnl_menuBar = new System.Windows.Forms.Panel();
			this.btn_off = new System.Windows.Forms.PictureBox();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btn_teacher_ingo = new System.Windows.Forms.Button();
			this.btn_email = new System.Windows.Forms.Button();
			this.btn_update_marks = new System.Windows.Forms.Button();
			this.btn_Enter_resul = new System.Windows.Forms.Button();
			this.btn_stu_rec = new System.Windows.Forms.Button();
			this.btn_dashboard = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pnl_container = new System.Windows.Forms.Panel();
			this.pnl_barTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).BeginInit();
			this.pnl_menuBar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_off)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_barTop
			// 
			this.pnl_barTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.pnl_barTop.Controls.Add(this.label1);
			this.pnl_barTop.Controls.Add(this.ovalPictureBox1);
			this.pnl_barTop.Controls.Add(this.btn_minimized);
			this.pnl_barTop.Controls.Add(this.btn_maximize);
			this.pnl_barTop.Controls.Add(this.btn_cancel_panel);
			this.pnl_barTop.Controls.Add(this.btn_resize);
			this.pnl_barTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnl_barTop.Location = new System.Drawing.Point(0, 0);
			this.pnl_barTop.Name = "pnl_barTop";
			this.pnl_barTop.Size = new System.Drawing.Size(1300, 40);
			this.pnl_barTop.TabIndex = 0;
			this.pnl_barTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_barTop_MouseDown);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(97, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Avatar";
			// 
			// ovalPictureBox1
			// 
			this.ovalPictureBox1.BackColor = System.Drawing.Color.DarkGray;
			this.ovalPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("ovalPictureBox1.Image")));
			this.ovalPictureBox1.Location = new System.Drawing.Point(33, 0);
			this.ovalPictureBox1.Name = "ovalPictureBox1";
			this.ovalPictureBox1.Size = new System.Drawing.Size(40, 40);
			this.ovalPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.ovalPictureBox1.TabIndex = 1;
			this.ovalPictureBox1.TabStop = false;
			// 
			// btn_minimized
			// 
			this.btn_minimized.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_minimized.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_minimized.Image = global::SGM.Properties.Resources.minus;
			this.btn_minimized.Location = new System.Drawing.Point(1178, 9);
			this.btn_minimized.Name = "btn_minimized";
			this.btn_minimized.Size = new System.Drawing.Size(25, 25);
			this.btn_minimized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_minimized.TabIndex = 0;
			this.btn_minimized.TabStop = false;
			this.btn_minimized.Click += new System.EventHandler(this.btn_minimized_Click);
			// 
			// btn_maximize
			// 
			this.btn_maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_maximize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_maximize.Image = global::SGM.Properties.Resources.maximize;
			this.btn_maximize.Location = new System.Drawing.Point(1220, 9);
			this.btn_maximize.Name = "btn_maximize";
			this.btn_maximize.Size = new System.Drawing.Size(25, 25);
			this.btn_maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_maximize.TabIndex = 0;
			this.btn_maximize.TabStop = false;
			this.btn_maximize.Click += new System.EventHandler(this.btn_maximize_Click);
			// 
			// btn_cancel_panel
			// 
			this.btn_cancel_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_cancel_panel.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_cancel_panel.Image = global::SGM.Properties.Resources.cancel;
			this.btn_cancel_panel.Location = new System.Drawing.Point(1263, 9);
			this.btn_cancel_panel.Name = "btn_cancel_panel";
			this.btn_cancel_panel.Size = new System.Drawing.Size(25, 25);
			this.btn_cancel_panel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_cancel_panel.TabIndex = 0;
			this.btn_cancel_panel.TabStop = false;
			this.btn_cancel_panel.Click += new System.EventHandler(this.btn_cancel_Click);
			// 
			// btn_resize
			// 
			this.btn_resize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_resize.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_resize.Image = global::SGM.Properties.Resources.scale;
			this.btn_resize.Location = new System.Drawing.Point(1220, 9);
			this.btn_resize.Name = "btn_resize";
			this.btn_resize.Size = new System.Drawing.Size(25, 25);
			this.btn_resize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.btn_resize.TabIndex = 0;
			this.btn_resize.TabStop = false;
			this.btn_resize.Click += new System.EventHandler(this.btn_resize_Click);
			// 
			// pnl_menuBar
			// 
			this.pnl_menuBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.pnl_menuBar.Controls.Add(this.btn_off);
			this.pnl_menuBar.Controls.Add(this.panel6);
			this.pnl_menuBar.Controls.Add(this.panel5);
			this.pnl_menuBar.Controls.Add(this.panel4);
			this.pnl_menuBar.Controls.Add(this.panel3);
			this.pnl_menuBar.Controls.Add(this.panel2);
			this.pnl_menuBar.Controls.Add(this.panel1);
			this.pnl_menuBar.Controls.Add(this.btn_teacher_ingo);
			this.pnl_menuBar.Controls.Add(this.btn_email);
			this.pnl_menuBar.Controls.Add(this.btn_update_marks);
			this.pnl_menuBar.Controls.Add(this.btn_Enter_resul);
			this.pnl_menuBar.Controls.Add(this.btn_stu_rec);
			this.pnl_menuBar.Controls.Add(this.btn_dashboard);
			this.pnl_menuBar.Controls.Add(this.pictureBox1);
			this.pnl_menuBar.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnl_menuBar.Location = new System.Drawing.Point(0, 40);
			this.pnl_menuBar.Name = "pnl_menuBar";
			this.pnl_menuBar.Size = new System.Drawing.Size(280, 610);
			this.pnl_menuBar.TabIndex = 1;
			// 
			// btn_off
			// 
			this.btn_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btn_off.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btn_off.Image = ((System.Drawing.Image)(resources.GetObject("btn_off.Image")));
			this.btn_off.Location = new System.Drawing.Point(12, 566);
			this.btn_off.Name = "btn_off";
			this.btn_off.Size = new System.Drawing.Size(32, 32);
			this.btn_off.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.btn_off.TabIndex = 3;
			this.btn_off.TabStop = false;
			this.btn_off.Click += new System.EventHandler(this.btn_off_Click);
			// 
			// panel6
			// 
			this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel6.Location = new System.Drawing.Point(1, 427);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(10, 46);
			this.panel6.TabIndex = 2;
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel5.Location = new System.Drawing.Point(1, 375);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(10, 46);
			this.panel5.TabIndex = 2;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel4.Location = new System.Drawing.Point(1, 323);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(10, 46);
			this.panel4.TabIndex = 2;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel3.Location = new System.Drawing.Point(1, 271);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(10, 46);
			this.panel3.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel2.Location = new System.Drawing.Point(1, 219);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(10, 46);
			this.panel2.TabIndex = 2;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.panel1.Location = new System.Drawing.Point(1, 167);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(10, 46);
			this.panel1.TabIndex = 2;
			// 
			// btn_teacher_ingo
			// 
			this.btn_teacher_ingo.FlatAppearance.BorderSize = 0;
			this.btn_teacher_ingo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_teacher_ingo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_teacher_ingo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_teacher_ingo.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_teacher_ingo.ForeColor = System.Drawing.Color.White;
			this.btn_teacher_ingo.Image = ((System.Drawing.Image)(resources.GetObject("btn_teacher_ingo.Image")));
			this.btn_teacher_ingo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_teacher_ingo.Location = new System.Drawing.Point(12, 427);
			this.btn_teacher_ingo.Name = "btn_teacher_ingo";
			this.btn_teacher_ingo.Size = new System.Drawing.Size(268, 46);
			this.btn_teacher_ingo.TabIndex = 1;
			this.btn_teacher_ingo.Text = "Teacher Info";
			this.btn_teacher_ingo.UseVisualStyleBackColor = true;
			this.btn_teacher_ingo.Click += new System.EventHandler(this.btn_teacher_ingo_Click);
			// 
			// btn_email
			// 
			this.btn_email.FlatAppearance.BorderSize = 0;
			this.btn_email.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_email.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_email.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_email.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_email.ForeColor = System.Drawing.Color.White;
			this.btn_email.Image = ((System.Drawing.Image)(resources.GetObject("btn_email.Image")));
			this.btn_email.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_email.Location = new System.Drawing.Point(12, 375);
			this.btn_email.Name = "btn_email";
			this.btn_email.Size = new System.Drawing.Size(268, 46);
			this.btn_email.TabIndex = 1;
			this.btn_email.Text = "Email";
			this.btn_email.UseVisualStyleBackColor = true;
			this.btn_email.Click += new System.EventHandler(this.btn_email_Click);
			// 
			// btn_update_marks
			// 
			this.btn_update_marks.FlatAppearance.BorderSize = 0;
			this.btn_update_marks.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_update_marks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_update_marks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_update_marks.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_update_marks.ForeColor = System.Drawing.Color.White;
			this.btn_update_marks.Image = ((System.Drawing.Image)(resources.GetObject("btn_update_marks.Image")));
			this.btn_update_marks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_update_marks.Location = new System.Drawing.Point(12, 323);
			this.btn_update_marks.Name = "btn_update_marks";
			this.btn_update_marks.Size = new System.Drawing.Size(268, 46);
			this.btn_update_marks.TabIndex = 1;
			this.btn_update_marks.Text = "Update Mark";
			this.btn_update_marks.UseVisualStyleBackColor = true;
			this.btn_update_marks.Click += new System.EventHandler(this.btn_update_marks_Click);
			// 
			// btn_Enter_resul
			// 
			this.btn_Enter_resul.FlatAppearance.BorderSize = 0;
			this.btn_Enter_resul.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_Enter_resul.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_Enter_resul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Enter_resul.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Enter_resul.ForeColor = System.Drawing.Color.White;
			this.btn_Enter_resul.Image = ((System.Drawing.Image)(resources.GetObject("btn_Enter_resul.Image")));
			this.btn_Enter_resul.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_Enter_resul.Location = new System.Drawing.Point(12, 271);
			this.btn_Enter_resul.Name = "btn_Enter_resul";
			this.btn_Enter_resul.Size = new System.Drawing.Size(268, 46);
			this.btn_Enter_resul.TabIndex = 1;
			this.btn_Enter_resul.Text = "Enter Result CSV";
			this.btn_Enter_resul.UseVisualStyleBackColor = true;
			this.btn_Enter_resul.Click += new System.EventHandler(this.btn_Enter_resul_Click);
			// 
			// btn_stu_rec
			// 
			this.btn_stu_rec.FlatAppearance.BorderSize = 0;
			this.btn_stu_rec.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_stu_rec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_stu_rec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_stu_rec.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_stu_rec.ForeColor = System.Drawing.Color.White;
			this.btn_stu_rec.Image = ((System.Drawing.Image)(resources.GetObject("btn_stu_rec.Image")));
			this.btn_stu_rec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_stu_rec.Location = new System.Drawing.Point(12, 219);
			this.btn_stu_rec.Name = "btn_stu_rec";
			this.btn_stu_rec.Size = new System.Drawing.Size(268, 46);
			this.btn_stu_rec.TabIndex = 1;
			this.btn_stu_rec.Text = "Student Records";
			this.btn_stu_rec.UseVisualStyleBackColor = true;
			this.btn_stu_rec.Click += new System.EventHandler(this.btn_stu_rec_Click);
			// 
			// btn_dashboard
			// 
			this.btn_dashboard.FlatAppearance.BorderSize = 0;
			this.btn_dashboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
			this.btn_dashboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(97)))), ((int)(((byte)(160)))));
			this.btn_dashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_dashboard.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_dashboard.ForeColor = System.Drawing.Color.White;
			this.btn_dashboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_dashboard.Image")));
			this.btn_dashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btn_dashboard.Location = new System.Drawing.Point(12, 167);
			this.btn_dashboard.Name = "btn_dashboard";
			this.btn_dashboard.Size = new System.Drawing.Size(268, 46);
			this.btn_dashboard.TabIndex = 1;
			this.btn_dashboard.Text = "Dashboard";
			this.btn_dashboard.UseVisualStyleBackColor = true;
			this.btn_dashboard.Click += new System.EventHandler(this.btn_dashboard_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::SGM.Properties.Resources._94facad9_b53f_469f_baf6_3eea74757acc;
			this.pictureBox1.Location = new System.Drawing.Point(0, -19);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(240, 201);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// pnl_container
			// 
			this.pnl_container.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl_container.ForeColor = System.Drawing.Color.White;
			this.pnl_container.Location = new System.Drawing.Point(280, 40);
			this.pnl_container.Name = "pnl_container";
			this.pnl_container.Size = new System.Drawing.Size(1020, 610);
			this.pnl_container.TabIndex = 2;
			// 
			// layout_teacher
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1300, 650);
			this.Controls.Add(this.pnl_container);
			this.Controls.Add(this.pnl_menuBar);
			this.Controls.Add(this.pnl_barTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "layout_teacher";
			this.Text = "Form1";
			this.pnl_barTop.ResumeLayout(false);
			this.pnl_barTop.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ovalPictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_minimized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_cancel_panel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btn_resize)).EndInit();
			this.pnl_menuBar.ResumeLayout(false);
			this.pnl_menuBar.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btn_off)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_barTop;
		private System.Windows.Forms.Panel pnl_menuBar;
		private System.Windows.Forms.Panel pnl_container;
		private System.Windows.Forms.PictureBox btn_cancel_panel;
		private System.Windows.Forms.PictureBox btn_minimized;
		private System.Windows.Forms.PictureBox btn_maximize;
		private System.Windows.Forms.PictureBox btn_resize;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btn_dashboard;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btn_Enter_resul;
		private System.Windows.Forms.Button btn_stu_rec;
		private System.Windows.Forms.PictureBox btn_off;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button btn_update_marks;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button btn_email;
		private OvalPictureBox ovalPictureBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Button btn_teacher_ingo;
	}
}

