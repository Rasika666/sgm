﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
namespace SGM
{
	public partial class Teacher_enter_result : Form
	{
		public Teacher_enter_result()
		{
			InitializeComponent();
		}

        private void btnchos_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog of1 = new OpenFileDialog();
                of1.Filter = "Excel Files | *.xlsx; *.xls; *.xlsm";
                if (of1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.txtpath.Text = of1.FileName;
                }
                string constr = "Provider= Microsoft.ACE.OLEDB.12.0;Data Source=" + txtpath.Text + ";Extended Properties= \"Excel 12.0; HDR=YES;\";";
                OleDbConnection con = new OleDbConnection(constr);
                con.Open();
                comboBox1.DataSource = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                comboBox1.DisplayMember = "TABLE_NAME";
                comboBox1.ValueMember = "TABLE_NAME";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                string constr = "Provider= Microsoft.ACE.OLEDB.12.0;Data Source=" + txtpath.Text + ";Extended Properties= \"Excel 12.0; HDR=YES;\";";
                OleDbConnection con = new OleDbConnection(constr);
                OleDbDataAdapter sda = new OleDbDataAdapter("select * from[" + comboBox1.SelectedValue + "]", con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    dataGridView1.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            int grade = Convert.ToInt32( dataGridView1.Rows[1].Cells[2].Value);
            database.DB.initializedDB();
           if (grade == 6)
            {
              
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    string sql =    "INSERT INTO sgm.marks (reg_no, grade, sem, Maths, Science, Sinhala, " +
                                    "Religion, English, History, Geography, Art, Health_Science, Tamil)" +
                                     " VALUES ('" + dataGridView1.Rows[i].Cells[1].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[2].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[3].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[4].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[5].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[6].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[7].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[8].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[9].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[10].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[11].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[12].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[13].Value + "')";
                    database.DB.putData(sql);
                }
            }
            else if(grade==7)
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    string sql = "INSERT INTO sgm.marks (reg_no, grade, sem, Maths, Science, Sinhala, " +
                                    "Religion, English, History, Geography, Art, Health_Science, Tamil, PTS)" +
                                     " VALUES ('" + dataGridView1.Rows[i].Cells[1].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[2].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[3].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[4].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[5].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[6].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[7].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[8].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[9].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[10].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[11].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[12].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[13].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[14].Value + "')";
                    database.DB.putData(sql);
                }
            }
            else if (grade == 8 || grade==9 || grade==10 || grade==11)
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    string sql = "INSERT INTO sgm.marks (reg_no, grade, sem, Maths, Science, Sinhala, " +
                                  "Religion, English, History, Geography, Art, Health_Science, Tamil, IT, " +
                                  "Dancing, Music, Civics)" +
                                     " VALUES ('" + dataGridView1.Rows[i].Cells[1].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[2].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[3].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[4].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[5].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[6].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[7].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[8].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[9].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[10].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[11].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[12].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[13].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[14].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[15].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[16].Value + "','" +
                                                    dataGridView1.Rows[i].Cells[17].Value + "')";
                    database.DB.putData(sql);
                }
            }
        
            database.DB.closeConnection();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
        }
    }
}
