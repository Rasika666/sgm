﻿namespace SGM
{
	partial class Stu_info
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stu_info));
			this.label1 = new System.Windows.Forms.Label();
			this.pnl_stuinfo_contend = new System.Windows.Forms.Panel();
			this.pnl_login = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.txt = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.txt_cuser = new System.Windows.Forms.TextBox();
			this.txt_nuser = new System.Windows.Forms.TextBox();
			this.txt_cpass = new System.Windows.Forms.TextBox();
			this.txt_npass = new System.Windows.Forms.TextBox();
			this.pnl_msg = new System.Windows.Forms.Panel();
			this.lbl_msg = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txt_conpass = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.txt_reg = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txt_address = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.txt_age = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txt_full_name = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.txt_name_init = new System.Windows.Forms.TextBox();
			this.txt_contact = new System.Windows.Forms.TextBox();
			this.pic_avator = new System.Windows.Forms.PictureBox();
			this.btn_browse = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.txt_mail = new System.Windows.Forms.TextBox();
			this.btn_login = new System.Windows.Forms.Button();
			this.btn_edit = new System.Windows.Forms.Button();
			this.btn_update = new System.Windows.Forms.Button();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.txt_leavedate = new System.Windows.Forms.TextBox();
			this.txt_enterdate = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.pnl_stuinfo_contend.SuspendLayout();
			this.pnl_login.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.tableLayoutPanel3.SuspendLayout();
			this.pnl_msg.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avator)).BeginInit();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(835, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Student Information";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// pnl_stuinfo_contend
			// 
			this.pnl_stuinfo_contend.Controls.Add(this.pnl_login);
			this.pnl_stuinfo_contend.Controls.Add(this.tableLayoutPanel1);
			this.pnl_stuinfo_contend.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl_stuinfo_contend.Location = new System.Drawing.Point(0, 29);
			this.pnl_stuinfo_contend.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
			this.pnl_stuinfo_contend.Name = "pnl_stuinfo_contend";
			this.pnl_stuinfo_contend.Size = new System.Drawing.Size(835, 487);
			this.pnl_stuinfo_contend.TabIndex = 1;
			// 
			// pnl_login
			// 
			this.pnl_login.Controls.Add(this.pictureBox1);
			this.pnl_login.Controls.Add(this.tableLayoutPanel3);
			this.pnl_login.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl_login.Location = new System.Drawing.Point(0, 0);
			this.pnl_login.Name = "pnl_login";
			this.pnl_login.Size = new System.Drawing.Size(835, 487);
			this.pnl_login.TabIndex = 6;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(801, 24);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(25, 25);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel3.ColumnCount = 4;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.42857F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
			this.tableLayoutPanel3.Controls.Add(this.label12, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.label13, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.label14, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.label15, 1, 1);
			this.tableLayoutPanel3.Controls.Add(this.txt, 1, 2);
			this.tableLayoutPanel3.Controls.Add(this.label17, 1, 3);
			this.tableLayoutPanel3.Controls.Add(this.button1, 2, 6);
			this.tableLayoutPanel3.Controls.Add(this.txt_cuser, 2, 0);
			this.tableLayoutPanel3.Controls.Add(this.txt_nuser, 2, 1);
			this.tableLayoutPanel3.Controls.Add(this.txt_cpass, 2, 2);
			this.tableLayoutPanel3.Controls.Add(this.txt_npass, 2, 3);
			this.tableLayoutPanel3.Controls.Add(this.pnl_msg, 2, 5);
			this.tableLayoutPanel3.Controls.Add(this.label16, 1, 4);
			this.tableLayoutPanel3.Controls.Add(this.txt_conpass, 2, 4);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 55);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 7;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(818, 356);
			this.tableLayoutPanel3.TabIndex = 2;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label12.Location = new System.Drawing.Point(3, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(4, 50);
			this.label12.TabIndex = 1;
			this.label12.Text = "Currnt User Name";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label13.Location = new System.Drawing.Point(3, 50);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(4, 50);
			this.label13.TabIndex = 1;
			this.label13.Text = "New User Name";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label14.Location = new System.Drawing.Point(13, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(193, 27);
			this.label14.TabIndex = 1;
			this.label14.Text = "Current User Name";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label15.Location = new System.Drawing.Point(13, 50);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(164, 27);
			this.label15.TabIndex = 1;
			this.label15.Text = "New User Name";
			// 
			// txt
			// 
			this.txt.AutoSize = true;
			this.txt.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.txt.Location = new System.Drawing.Point(13, 100);
			this.txt.Name = "txt";
			this.txt.Size = new System.Drawing.Size(179, 27);
			this.txt.TabIndex = 1;
			this.txt.Text = "Current Password";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label17.Location = new System.Drawing.Point(13, 150);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(150, 27);
			this.label17.TabIndex = 1;
			this.label17.Text = "New password";
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.button1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.button1.Location = new System.Drawing.Point(507, 315);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(207, 37);
			this.button1.TabIndex = 2;
			this.button1.Text = "Save";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txt_cuser
			// 
			this.txt_cuser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_cuser.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_cuser.Location = new System.Drawing.Point(215, 3);
			this.txt_cuser.Name = "txt_cuser";
			this.txt_cuser.Size = new System.Drawing.Size(499, 29);
			this.txt_cuser.TabIndex = 3;
			// 
			// txt_nuser
			// 
			this.txt_nuser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_nuser.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_nuser.Location = new System.Drawing.Point(215, 53);
			this.txt_nuser.Name = "txt_nuser";
			this.txt_nuser.Size = new System.Drawing.Size(499, 29);
			this.txt_nuser.TabIndex = 3;
			// 
			// txt_cpass
			// 
			this.txt_cpass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_cpass.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_cpass.Location = new System.Drawing.Point(215, 103);
			this.txt_cpass.Name = "txt_cpass";
			this.txt_cpass.PasswordChar = '*';
			this.txt_cpass.Size = new System.Drawing.Size(499, 29);
			this.txt_cpass.TabIndex = 3;
			// 
			// txt_npass
			// 
			this.txt_npass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_npass.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_npass.Location = new System.Drawing.Point(215, 153);
			this.txt_npass.Name = "txt_npass";
			this.txt_npass.PasswordChar = '*';
			this.txt_npass.Size = new System.Drawing.Size(499, 29);
			this.txt_npass.TabIndex = 3;
			// 
			// pnl_msg
			// 
			this.pnl_msg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pnl_msg.BackColor = System.Drawing.Color.Red;
			this.pnl_msg.Controls.Add(this.lbl_msg);
			this.pnl_msg.Controls.Add(this.pictureBox2);
			this.pnl_msg.Location = new System.Drawing.Point(215, 253);
			this.pnl_msg.Name = "pnl_msg";
			this.pnl_msg.Size = new System.Drawing.Size(499, 56);
			this.pnl_msg.TabIndex = 4;
			// 
			// lbl_msg
			// 
			this.lbl_msg.AutoSize = true;
			this.lbl_msg.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_msg.ForeColor = System.Drawing.Color.White;
			this.lbl_msg.Location = new System.Drawing.Point(70, 8);
			this.lbl_msg.Name = "lbl_msg";
			this.lbl_msg.Size = new System.Drawing.Size(0, 17);
			this.lbl_msg.TabIndex = 1;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(506, 3);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(16, 16);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox2.TabIndex = 0;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label16.Location = new System.Drawing.Point(13, 200);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(184, 27);
			this.label16.TabIndex = 1;
			this.label16.Text = "Confirm password";
			// 
			// txt_conpass
			// 
			this.txt_conpass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_conpass.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_conpass.Location = new System.Drawing.Point(215, 203);
			this.txt_conpass.Name = "txt_conpass";
			this.txt_conpass.PasswordChar = '*';
			this.txt_conpass.Size = new System.Drawing.Size(499, 29);
			this.txt_conpass.TabIndex = 3;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Controls.Add(this.txt_reg, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_address, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_age, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
			this.tableLayoutPanel1.Controls.Add(this.txt_full_name, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
			this.tableLayoutPanel1.Controls.Add(this.txt_name_init, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.txt_contact, 1, 7);
			this.tableLayoutPanel1.Controls.Add(this.pic_avator, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.btn_browse, 2, 6);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.txt_mail, 1, 8);
			this.tableLayoutPanel1.Controls.Add(this.btn_login, 2, 8);
			this.tableLayoutPanel1.Controls.Add(this.btn_edit, 1, 10);
			this.tableLayoutPanel1.Controls.Add(this.btn_update, 2, 10);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 9);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 32);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 11;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(832, 426);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// txt_reg
			// 
			this.txt_reg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_reg.Enabled = false;
			this.txt_reg.Location = new System.Drawing.Point(211, 3);
			this.txt_reg.Name = "txt_reg";
			this.txt_reg.Size = new System.Drawing.Size(410, 22);
			this.txt_reg.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label2.Location = new System.Drawing.Point(5, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 20);
			this.label2.TabIndex = 0;
			this.label2.Text = "Reg No";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label9.Location = new System.Drawing.Point(5, 296);
			this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(57, 20);
			this.label9.TabIndex = 0;
			this.label9.Text = "E-mail";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label3.Location = new System.Drawing.Point(5, 37);
			this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(132, 20);
			this.label3.TabIndex = 0;
			this.label3.Text = "Name with Initial";
			// 
			// txt_address
			// 
			this.txt_address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_address.Location = new System.Drawing.Point(211, 188);
			this.txt_address.Multiline = true;
			this.txt_address.Name = "txt_address";
			this.tableLayoutPanel1.SetRowSpan(this.txt_address, 2);
			this.txt_address.Size = new System.Drawing.Size(410, 68);
			this.txt_address.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label5.Location = new System.Drawing.Point(5, 111);
			this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(38, 20);
			this.label5.TabIndex = 0;
			this.label5.Text = "Age";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label4.Location = new System.Drawing.Point(5, 74);
			this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(85, 20);
			this.label4.TabIndex = 0;
			this.label4.Text = "Full Name";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label6.Location = new System.Drawing.Point(5, 148);
			this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(37, 20);
			this.label6.TabIndex = 0;
			this.label6.Text = "Sex";
			// 
			// txt_age
			// 
			this.txt_age.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_age.Location = new System.Drawing.Point(211, 114);
			this.txt_age.Name = "txt_age";
			this.txt_age.Size = new System.Drawing.Size(410, 22);
			this.txt_age.TabIndex = 2;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label7.Location = new System.Drawing.Point(5, 185);
			this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(71, 20);
			this.label7.TabIndex = 0;
			this.label7.Text = "Address";
			// 
			// txt_full_name
			// 
			this.txt_full_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_full_name.Location = new System.Drawing.Point(211, 77);
			this.txt_full_name.Name = "txt_full_name";
			this.txt_full_name.Size = new System.Drawing.Size(410, 22);
			this.txt_full_name.TabIndex = 2;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label8.Location = new System.Drawing.Point(5, 259);
			this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(93, 20);
			this.label8.TabIndex = 0;
			this.label8.Text = "Contact No";
			// 
			// txt_name_init
			// 
			this.txt_name_init.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_name_init.Location = new System.Drawing.Point(211, 40);
			this.txt_name_init.Name = "txt_name_init";
			this.txt_name_init.Size = new System.Drawing.Size(410, 22);
			this.txt_name_init.TabIndex = 2;
			// 
			// txt_contact
			// 
			this.txt_contact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_contact.Location = new System.Drawing.Point(211, 262);
			this.txt_contact.Name = "txt_contact";
			this.txt_contact.Size = new System.Drawing.Size(410, 22);
			this.txt_contact.TabIndex = 2;
			// 
			// pic_avator
			// 
			this.pic_avator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pic_avator.Image = ((System.Drawing.Image)(resources.GetObject("pic_avator.Image")));
			this.pic_avator.Location = new System.Drawing.Point(627, 3);
			this.pic_avator.Name = "pic_avator";
			this.tableLayoutPanel1.SetRowSpan(this.pic_avator, 5);
			this.pic_avator.Size = new System.Drawing.Size(202, 179);
			this.pic_avator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pic_avator.TabIndex = 1;
			this.pic_avator.TabStop = false;
			// 
			// btn_browse
			// 
			this.btn_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_browse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_browse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_browse.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.btn_browse.Location = new System.Drawing.Point(627, 225);
			this.btn_browse.Name = "btn_browse";
			this.btn_browse.Size = new System.Drawing.Size(202, 31);
			this.btn_browse.TabIndex = 3;
			this.btn_browse.Text = "Browse";
			this.btn_browse.UseVisualStyleBackColor = false;
			this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.radioButton2);
			this.panel1.Controls.Add(this.radioButton1);
			this.panel1.Location = new System.Drawing.Point(211, 151);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(410, 31);
			this.panel1.TabIndex = 4;
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioButton2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.radioButton2.Location = new System.Drawing.Point(208, 3);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(85, 24);
			this.radioButton2.TabIndex = 0;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Female";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.radioButton1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.radioButton1.Location = new System.Drawing.Point(20, 3);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(66, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Male";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// txt_mail
			// 
			this.txt_mail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txt_mail.Location = new System.Drawing.Point(211, 299);
			this.txt_mail.Name = "txt_mail";
			this.txt_mail.Size = new System.Drawing.Size(410, 22);
			this.txt_mail.TabIndex = 2;
			// 
			// btn_login
			// 
			this.btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_login.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_login.Location = new System.Drawing.Point(627, 299);
			this.btn_login.Name = "btn_login";
			this.btn_login.Size = new System.Drawing.Size(202, 31);
			this.btn_login.TabIndex = 5;
			this.btn_login.Text = "Edit Login";
			this.btn_login.UseVisualStyleBackColor = false;
			this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
			// 
			// btn_edit
			// 
			this.btn_edit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_edit.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.btn_edit.Location = new System.Drawing.Point(211, 385);
			this.btn_edit.Name = "btn_edit";
			this.btn_edit.Size = new System.Drawing.Size(410, 38);
			this.btn_edit.TabIndex = 3;
			this.btn_edit.Text = "Edit";
			this.btn_edit.UseVisualStyleBackColor = false;
			this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
			// 
			// btn_update
			// 
			this.btn_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(150)))), ((int)(((byte)(215)))));
			this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_update.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btn_update.Location = new System.Drawing.Point(633, 373);
			this.btn_update.Name = "btn_update";
			this.btn_update.Size = new System.Drawing.Size(196, 50);
			this.btn_update.TabIndex = 5;
			this.btn_update.Text = "Update";
			this.btn_update.UseVisualStyleBackColor = false;
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.Controls.Add(this.txt_leavedate, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.txt_enterdate, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.label11, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.label10, 2, 0);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 336);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(826, 31);
			this.tableLayoutPanel2.TabIndex = 6;
			// 
			// txt_leavedate
			// 
			this.txt_leavedate.Enabled = false;
			this.txt_leavedate.Location = new System.Drawing.Point(621, 3);
			this.txt_leavedate.Name = "txt_leavedate";
			this.txt_leavedate.Size = new System.Drawing.Size(202, 22);
			this.txt_leavedate.TabIndex = 0;
			// 
			// txt_enterdate
			// 
			this.txt_enterdate.Enabled = false;
			this.txt_enterdate.Location = new System.Drawing.Point(209, 3);
			this.txt_enterdate.Name = "txt_enterdate";
			this.txt_enterdate.Size = new System.Drawing.Size(200, 22);
			this.txt_enterdate.TabIndex = 1;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label11.Location = new System.Drawing.Point(5, 0);
			this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(90, 20);
			this.label11.TabIndex = 0;
			this.label11.Text = "Enter Date";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
			this.label10.Location = new System.Drawing.Point(417, 0);
			this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 3, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(95, 20);
			this.label10.TabIndex = 0;
			this.label10.Text = "Leave Date";
			// 
			// Stu_info
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
			this.ClientSize = new System.Drawing.Size(835, 516);
			this.Controls.Add(this.pnl_stuinfo_contend);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Stu_info";
			this.Text = "Stu_info";
			this.pnl_stuinfo_contend.ResumeLayout(false);
			this.pnl_login.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.pnl_msg.ResumeLayout(false);
			this.pnl_msg.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pic_avator)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel pnl_stuinfo_contend;
		private System.Windows.Forms.TextBox txt_address;
		private System.Windows.Forms.TextBox txt_mail;
		private System.Windows.Forms.TextBox txt_age;
		private System.Windows.Forms.TextBox txt_full_name;
		private System.Windows.Forms.TextBox txt_name_init;
		private System.Windows.Forms.TextBox txt_reg;
		private System.Windows.Forms.PictureBox pic_avator;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btn_browse;
		private System.Windows.Forms.TextBox txt_contact;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.Button btn_edit;
		private System.Windows.Forms.Button btn_login;
		private System.Windows.Forms.Button btn_update;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TextBox txt_leavedate;
		private System.Windows.Forms.TextBox txt_enterdate;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Panel pnl_login;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label txt;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txt_cuser;
		private System.Windows.Forms.TextBox txt_nuser;
		private System.Windows.Forms.TextBox txt_cpass;
		private System.Windows.Forms.TextBox txt_npass;
		private System.Windows.Forms.Panel pnl_msg;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lbl_msg;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txt_conpass;
	}
}